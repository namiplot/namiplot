/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

/*********** TEMPLATE FUNCTIONS ***********/

// Default behavior: When destructed, keep the plot on screen.
class window
{
    // FIXME: Thread support. Shared_ptr?
    chart_ptr chart;

    std::jthread X_thread;

    // Takes ownership of x11_dpy and cairo_sfc.
    void thread_main(chart_ptr         thread_chart,
                     Display         * x11_dpy,
                     Atom              wmDeleteMessage,
                     cairo_surface_t * cairo_sfc,
                     float             screen_framerate);

    void setup_window();

public:
    window() = delete;

    template <Namichart C>
    window(C const& plot_chart)
      : chart {std::make_unique<C>(plot_chart)}
    { setup_window(); }

    // TODO void replace_chart() ..., get_chart() ...

    ~window();

public:
    bool active() const
    { return !X_thread.get_stop_token().stop_requested(); }
};


