/* Copyright 2020-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {

void
namiPNG(base_chart const& plot,
        std::filesystem::path output,
        canvas_size_t size
        )
{
    cairo_surface_t * cairo_sfc = cairo_image_surface_create(
                CAIRO_FORMAT_ARGB32, size.width, size.height);

    plot.render(cairo_sfc, {});

    cairo_status_t error_code =
        cairo_surface_write_to_png(cairo_sfc, output.c_str());

    cairo_surface_destroy(cairo_sfc);

    if (error_code != CAIRO_STATUS_SUCCESS)
    {
        std::string error = "namiPNG() failed: ";
        error += cairo_status_to_string(error_code);
        throw std::runtime_error(error);
    }
}

} // namespace nami

#ifdef TEST //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace test
{

void test()
{
    // TODO
    // Check that a file is generated and is of the correct type.
}

} // namespace test
#endif // TEST

