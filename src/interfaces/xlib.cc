/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {

void
window:: setup_window()
{
    auto const& canvas = chart->theme().canvas;

    Display * x11_dpy = nullptr;
    Atom wmDeleteMessage;

    cairo_surface_t * cairo_sfc = nullptr;

    /*****  X11 Connection  *****/

    // 1. Connect to the X server.
    if ((x11_dpy = XOpenDisplay(NULL)) == NULL)
        throw -1;

    // 2. Create a window.
    Drawable dwindow = XCreateWindow(
            x11_dpy,
            DefaultRootWindow(x11_dpy), // The parent window
            0,                          // Top-left coordinates relative to
            0,                          // the parent's window
            canvas.width,               // Window width
            canvas.height,              // Window height
            0,                          // Width of window's border
            0,                          // Depth
            CopyFromParent,             // Class
            CopyFromParent,             // Visual (Color depth, etc.)
            0,                          // Value mask
            nullptr);                   // Attributes
    XStoreName(x11_dpy, dwindow, "namiplot render window");

    // 3. Choose input events.
    XSelectInput(x11_dpy, dwindow, ExposureMask | StructureNotifyMask);

    // 4. Hendler for delete window.
    wmDeleteMessage = XInternAtom(x11_dpy, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(x11_dpy, dwindow, &wmDeleteMessage, 1);

    // 4. Display the window.
    XMapWindow(x11_dpy, dwindow);

    // 5. Get screen framerate
    float fps = XRRConfigCurrentRate( XRRGetScreenInfo(x11_dpy, dwindow) );

    // 6. Create Cairo Surface
    cairo_sfc = cairo_xlib_surface_create(
            x11_dpy,
            dwindow,
            DefaultVisual(x11_dpy, DefaultScreen(x11_dpy)),
            canvas.width,
            canvas.height
            );
    cairo_xlib_surface_set_size(
            cairo_sfc,
            canvas.width,
            canvas.height
            );

    /*****  Thread  *****/

    try {
        // Transfer ownerships
        X_thread = std::jthread(&window::thread_main, this, chart,
                x11_dpy, wmDeleteMessage, cairo_sfc, fps);
        std::this_thread::yield();
    } catch (...) {
        cairo_surface_destroy(cairo_sfc);
        XCloseDisplay(x11_dpy);
        throw;
    }

}

window:: ~window()
{
    X_thread.detach();
}

void
window:: thread_main(chart_ptr         thread_chart,
                     Display         * x11_dpy,
                     Atom              wmDeleteMessage,
                     cairo_surface_t * cairo_sfc,
                     float             screen_framerate)
{
    const auto poll_wait = std::chrono::nanoseconds{int(1.e9/screen_framerate)};
    animation_info animation_data =
        { .frame_count = -1u, .framerate = screen_framerate };

    std::stop_source stop_source = X_thread.get_stop_source();
    auto now                     = std::chrono::steady_clock::now;
    auto next_refresh            = now();

    while (!stop_source.stop_requested())
    {
        while (next_refresh < now())
        {
            next_refresh += poll_wait;
            animation_data.frame_count++;
        }

        /* Xlib event queue processing */
        while (XPending(x11_dpy))
        {
            XEvent e;
            XNextEvent(x11_dpy, &e);

            switch (e.type)
            {
                //case Expose:
                //update_planned = true;
                //break;
            case ConfigureNotify:
                cairo_xlib_surface_set_size(cairo_sfc,
                        e.xconfigure.width,
                        e.xconfigure.height);
                break;
            case ClientMessage:
                if (e.xclient.data.l[0] == (long int)wmDeleteMessage)
                {
                    stop_source.request_stop();
                    goto stop;
                }
                break;
            default:
                break;
            }
        }

        try
        {
            chart_ptr CoW_chart = thread_chart; // FIXME Better use shared_ptr
            if (CoW_chart)
                CoW_chart->render(cairo_sfc, animation_data);
        }
        catch(std::exception const& error) {
            std::cerr << error.what() << "\n";
            goto stop;
        }

        XFlush(x11_dpy);
        std::this_thread::sleep_until(next_refresh);
    }

stop:
    cairo_surface_destroy(cairo_sfc);
    XCloseDisplay(x11_dpy);
}

} // namespace nami

