/* Copyright 2020-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */


//__________________________________ OPENCV __________________________________||

template <Namidata_openCV Data>
class data_adaptor<Data> final : public data_t
{
    Data data;

public:
    using data_type = Data;

public:
    data_adaptor() = default;

    data_adaptor(Data const& data_copy)
    {
        // WARNING: OpenCV defined CV_64F with macros, which are not available
        // in modules. The value 6 is taken from OpenCV's source code.
        data_copy.convertTo(data, /* CV_64F = */ 6);
        if (data.dims > 2)
            throw std::runtime_error("Namiplot only supports OpenCV "
                    "matrices up to two dimensions");
        if (!data.empty())
        {
            auto minmax = std::minmax_element(data.template begin<double>(),
                                              data.template end  <double>());
            min_element = *minmax.first;
            max_element = *minmax.second;
        }
    }

    POLYMORPHIC_CLONE_OVERRIDE(data_adaptor)

public: /* Interface implementation */
    size_t size() const override
    {
        return data.total();
    }

    size_t rows() const override
    {
        return data.rows;
    }

    size_t cols() const override
    {
        return data.cols;
    }

    bool  empty() const override
    {
        return data.empty();
    }

    // Accessors
    double at(size_t index) const override
    {
        size_t row = index / cols();
        size_t col = index % cols();
        return at(row, col);
    }

    double at(size_t row, size_t col) const override
    {
        return data.template at<double>(row, col);
    }
};
