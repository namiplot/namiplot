/* Copyright 2020-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>

#ifdef TEST //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#include <deque>

namespace test
{

using namespace nami;

void test_vector()
{
    static_assert(std::semiregular<data_adaptor<std::vector<int>> >);

    std::vector<double> vector_double { -3, -2, -1, 0, 1, 2, 3 };
    std::cout << "Testing vector<double>" << std::endl;
    test::test_namidata(vector_double);

    const std::vector<int> vector_int { -3, -2, -1, 0, 1, 2, 3 };
    std::cout << "Testing const vector<int>" << std::endl;
    test::test_namidata(vector_int);

    std::deque<double> deque_double { -3, -2, -1, 0, 1, 2, 3 };
    std::cout << "Testing deque<double>" << std::endl;
    test::test_namidata(deque_double);
}

void test_vector_vector()
{
    static_assert(std::semiregular<data_adaptor<
            std::vector<std::vector<int>> >>);

    std::vector<std::vector<double>> m_d
      { {11, 12, 13, 14 }
      , {21, 22, 23, 24 }
      , {31, 32, 33, 34 } };
    std::vector<std::vector<double>> v_d { { -3, -2, -1, 0, 1, 2, 3 } };
    std::cout << "Testing vector<vector<double>>" << std::endl;
    test::test_namidata(m_d, v_d);

    const std::vector<std::deque<int>> m_i
      { {11, 12, 13, 14 }
      , {21, 22, 23, 24 }
      , {31, 32, 33, 34 } };
    const std::vector<std::deque<int>> v_i { { -3, -2, -1, 0, 1, 2, 3 } };
    std::cout << "Testing const vector<deque<int>>" << std::endl;
    test::test_namidata(m_i, v_i);

    const std::deque<std::vector<char>> m_c
      { {11, 12, 13, 14 }
      , {21, 22, 23, 24 }
      , {31, 32, 33, 34 } };
    const std::deque<std::vector<char>> v_c { { -3, -2, -1, 0, 1, 2, 3 } };
    std::cout << "Testing const deque<vector<char>>" << std::endl;
    test::test_namidata(m_c, v_c);


    std::vector<std::vector<double>> inc = m_d;
    std::cout << "Testing consistency of internal dimensions" << std::endl;
    data_adaptor<std::vector<std::vector<double>>> ok {inc};
    inc[2].push_back(35);
    ASSERT_THROW(data_adaptor<std::vector<std::vector<double>>> fail {inc} );
}

void test()
{
    test_vector();
    test_vector_vector();
}

} // namespace test
#endif // TEST

