/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

//________________________________ ARMADILLO _________________________________||

template <Namidata_armadillo Data>
class data_adaptor<Data> final : public data_t
{
    Data data;

public:
    using data_type = Data;

public:
    data_adaptor() = default;

    data_adaptor(Data const& data_copy)
    {
        data = data_copy;
        if (!data.is_empty())
        {
            min_element = data.min();
            max_element = data.max();
        }
    }

    POLYMORPHIC_CLONE_OVERRIDE(data_adaptor)

public: /* Interface implementation */
    size_t size() const override
    {
        return data.n_elem;
    }

    size_t rows() const override
    {
        return data.n_rows;
    }

    size_t cols() const override
    {
        return data.n_cols;
    }

    bool  empty() const override
    {
        return data.is_empty();
    }

    // Accessors
    double at(size_t index) const override
    {
        size_t row = index / cols();
        size_t col = index % cols();
        return data.at(row, col);
    }

    double at(size_t row, size_t col) const override
    {
        return data.at(row, col);
    }
};

// TODO blaze eigen
//__________________________________ BLAZE ___________________________________||
//__________________________________ EIGEN ___________________________________||
//_________________________________ XTENSOR __________________________________||
