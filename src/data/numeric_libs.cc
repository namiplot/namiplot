/* Copyright 2020-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>


#ifdef TEST //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#include <armadillo>

namespace test
{

void test_armadillo()
{

    std::cout << "Testing ArmaVersion " << arma::arma_version::as_string()
        << std::endl;
    static_assert(std::semiregular<arma::Mat<int>>);
    static_assert(std::semiregular<arma::mat>);
    static_assert(std::semiregular<arma::sp_mat>);

    std::cout << "Testing arma::mat and arma::rowvec" << std::endl;
    arma::mat m_d
      { {11, 12, 13, 14 }
      , {21, 22, 23, 24 }
      , {31, 32, 33, 34 } };
    arma::rowvec v_d { -3, -2, -1, 0, 1, 2, 3 };
    test::test_namidata<arma::mat>(m_d, v_d);

    std::cout << "Testing const arma::Mat<int> and const arma::Row<int>"
        << std::endl;
    const arma::Mat<int> m_i
      { {11, 12, 13, 14 }
      , {21, 22, 23, 24 }
      , {31, 32, 33, 34 } };
    const arma::Row<int> v_i { -3, -2, -1, 0, 1, 2, 3 };
    test::test_namidata<arma::Mat<int>>(m_i, v_i);

    std::cout << "Testing sparse matrices arma::sp_mat" << std::endl;
    arma::sp_mat m_s {m_d};
    arma::sp_mat v_s {v_d};
    test::test_namidata(m_s, v_s);
}

void test()
{
    test_armadillo();
}

} // namespace test
#endif // TEST

