/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

//_________________________________ FUNCTION _________________________________||

template <Namifunction Fun>
class data_adaptor<Fun> final : public data_t
{
    Fun fun;
    data_cptr t; // x=f(t) y=f'(t)

public:
    using data_type = Fun;

public:
    data_adaptor(Fun function, data_cptr && t_ptr)
      : fun{ function }
      , t  { std::move(t_ptr) }
    {
        if (t->empty())
            throw std::range_error("No data for Namifunction");
        else
        {
            double compute_min = std::numeric_limits<double>::max();
            double compute_max = std::numeric_limits<double>::lowest();
            for (unsigned i = 0; i < t->size(); i++)
            {
                const double value_at_i = fun(t->at(i));
                compute_min = std::min( compute_min, value_at_i );
                compute_max = std::max( compute_max, value_at_i );
            }
            min_element = compute_min;
            max_element = compute_max;
        }
    }

    template <Namidata T>
    data_adaptor(Fun function, T const& t_parameter)
      : data_adaptor(function, std::make_unique<data_adaptor<T>>(t_parameter))
    { }

    POLYMORPHIC_CLONE_OVERRIDE(data_adaptor)

public: /* Interface implementation */
    size_t size() const override
    {
        return t->size();
    }

    size_t rows() const override
    {
        return t->rows();
    }

    size_t cols() const override
    {
        return t->cols();
    }

    bool empty() const override
    {
        return t->empty();
    }

    // Accessors
    double at(size_t index) const override
    {
        return fun(t->at(index));
    }

    double at(size_t row, size_t col) const override
    {
        return fun(t->at(row, col));
    }
};

// FIXME: static_assert(std::semiregular< data_adaptor<Function> >);


//__________________________________ VECTOR __________________________________||

template <Namidata_vector Data>
class data_adaptor<Data> final : public data_t
{
    Data data;

public:
    using data_type = Data;

public:
    data_adaptor() = default;

    data_adaptor(Data const& data_copy)
    {
        data = data_copy;
        if (!data.empty())
        {
            auto [itmin, itmax] = std::minmax_element(data.begin(), data.end());
            min_element = *itmin;
            max_element = *itmax;
        }
    }

    POLYMORPHIC_CLONE_OVERRIDE(data_adaptor)

public: /* Interface implementation */
    size_t size() const override
    {
        return  data.size();
    }

    size_t rows() const override
    {
        return !data.empty();
    }

    size_t cols() const override
    {
        return  data.size();
    }

    bool  empty() const override
    {
        return  data.empty();
    }

    // Accessors
    double at(size_t index) const override
    {
        return data[index];
    }

    double at(size_t row, size_t col) const override
    {
        return data[col];
    }
};


//______________________________ VECTOR_VECTOR _______________________________||

template <Namidata_vector_vector Data>
class data_adaptor<Data> final : public data_t
{
    Data data;

    void assert_all_rows_same_size(Data const& mat) const
    {
        if (mat.empty())
            return;
        unsigned rows = mat[0].size();
        for (auto & col : mat)
            if (col.size() != rows)
                throw std::range_error( "namiplot: "
                        "matrix has inconsistent internal dimensions");
    }

public:
    using data_type = Data;

public: /* Semiregular type traits */
    data_adaptor() = default;

public: /* Type specific constructors and assignments */
    data_adaptor(Data const& data_copy)
    {
        *this = data_copy;
    }

    data_adaptor & operator = (Data const& data_copy)
    {
        assert_all_rows_same_size(data_copy);
        data = data_copy;

        if (!data_copy.empty() && !data_copy[0].empty())
        {
            double compute_min = std::numeric_limits<double>::max();
            double compute_max = std::numeric_limits<double>::lowest();
            for (auto & row : data)
            {
                const auto [it_min, it_max] =
                    std::minmax_element(row.begin(), row.end());
                compute_min = std::min<double>( compute_min, *it_min );
                compute_max = std::max<double>( compute_max, *it_max );
            }
            min_element = compute_min;
            max_element = compute_max;
        }

        return *this;
    }

    POLYMORPHIC_CLONE_OVERRIDE(data_adaptor)

public: /* Interface implementation */
    size_t size() const override
    {
        return data.empty() ? 0 : data.size() * data[0].size();
    }

    size_t rows() const override
    {
        return data.empty() ? 0 : data.size();
    }

    size_t cols() const override
    {
        return data.empty() ? 0 : data[0].size();
    }

    bool empty() const override
    {
        return data.empty() ? true : data[0].empty();
    }

    // Accessors
    double at(size_t index) const override
    {
        size_t row = index / cols();
        size_t col = index % cols();
        return data[row][col];
    }

    double at(size_t row, size_t col) const override
    {
        return data[row][col];
    }
};


// TODO
//___________________________________ FILE ___________________________________||
// TODO


