/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

//______________________________ DATA CONCEPTS _______________________________||
template <typename D>
concept Namidata_vector =
       std::semiregular<D>
    && requires (D const d) {
           {d.size()}     -> std::same_as<size_t>;
           {d.empty()}    -> std::same_as<bool>;

           {d[size_t(0)]} -> std::convertible_to<double>;

           {d.begin()}    -> std::same_as<typename D::const_iterator>;
           {d.end()}      -> std::same_as<typename D::const_iterator>;
           {*d.begin()}   -> std::convertible_to<double>;
       }
    && !requires (D d) {
           {d(0.0)};
       };

template <typename D>
concept Namidata_vector_ref =
       std::is_lvalue_reference_v<D>
    && Namidata_vector<std::remove_cvref_t<D>>;

// Row major ordering (inner vector contains whole rows)
template <typename D>
concept Namidata_vector_vector =
       std::semiregular<D>
    && requires (D const d) {
           {d.size()}     -> std::same_as<size_t>;
           {d.empty()}    -> std::same_as<bool>;

           {d[size_t(0)]} -> Namidata_vector_ref;

           {d.begin()}    -> std::same_as<typename D::const_iterator>;
           {d.end()}      -> std::same_as<typename D::const_iterator>;
           {*d.begin()}   -> Namidata_vector_ref;
       };

template <typename D>
concept Namidata_armadillo =
       std::semiregular<D>
    && requires (D const d) {
           {d.n_elem}     -> std::convertible_to<unsigned>;
           {d.n_rows}     -> std::convertible_to<unsigned>;
           {d.n_cols}     -> std::convertible_to<unsigned>;
           {d.is_empty()} -> std::same_as<bool>;

           {d.at(0, 0)}   -> std::convertible_to<double>;

           {d.begin()}    -> std::same_as<typename D::const_iterator>;
           {d.end()}      -> std::same_as<typename D::const_iterator>;
           {*d.begin()}   -> std::convertible_to<double>;
       };

template <typename D>
concept Namidata_openCV =
       std::semiregular<D>
    && requires (D const& d) {
           {d.convertTo(d, 6)} -> std::same_as<void>;

           {d.total()} -> std::convertible_to<unsigned>;
           {d.rows}    -> std::convertible_to<unsigned>;
           {d.cols}    -> std::convertible_to<unsigned>;
           {d.dims}    -> std::convertible_to<unsigned>;
           {d.empty()} -> std::same_as<bool>;

           {d.template at<double>(0, 0)} -> std::convertible_to<double>;

           { d.template begin<double>()};
           { d.template end  <double>()};
           {*d.template begin<double>()} -> std::convertible_to<double>;
       };


//______________________________ NAMI CONCEPTS _______________________________||

template <typename D>
concept Namidata =
           Namidata_vector        <D>
        || Namidata_vector_vector <D>
        || Namidata_armadillo     <D>
        || Namidata_openCV        <D>
        ;

//_____________________________ NAMI FUNCTIONS _______________________________||


template <typename D>
concept Namifunction =
       std::invocable<D, double>
    && !Namidata<D>
    && requires (D const d, double datum) {
           {d(datum)} -> std::convertible_to<double>;
       };


//____________________________ VIRTUAL INTERFACE _____________________________||

class data_t
{
protected:
    double min_element = 0.;
    double max_element = 10.;

public:
    double min() const { return min_element; }
    double max() const { return max_element; }

public:
    // Size
    virtual size_t size() const = 0;
    virtual size_t rows() const = 0;
    virtual size_t cols() const = 0;
    virtual bool  empty() const = 0;
    // Accessors
    virtual double at(size_t index)           const = 0;
    virtual double at(size_t row, size_t col) const = 0;

    [[nodiscard("Ownership transferred to caller")]]
    virtual data_t * clone() const = 0;
    virtual ~data_t() { }
};

using data_cptr = polymorphic_ptr<const data_t>;

template <typename Data>
class data_adaptor final
{
    static_assert(Namidata<Data>, "This numeric type is not supported. "
            "Please submit a feature request at namiplot.gitlab.io");
};


//____________________________ NAMIDATA TESTS ________________________________||

#ifdef TEST //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

} // namespace nami
namespace test {
using namespace nami;

template <Namidata T>
void test_namidata_default(data_adaptor<T> const& zeroed)
{
    ASSERT(zeroed.size() == 0);
    ASSERT(zeroed.rows() == 0);
    ASSERT(zeroed.cols() == 0);
    ASSERT(zeroed.empty() == true);
    //ASSERT_THROW(zeroed.at(0));
    //ASSERT_THROW(zeroed.at(0,0));
    ASSERT(zeroed.min() == 0);
    ASSERT(zeroed.max() == 10);
}

template <Namidata T>
void test_namidata(T const& cvec)
{
    ASSERT(Namidata<T>);

    ASSERT(std::semiregular<data_adaptor<T>>);
    ASSERT(std::is_same_v<T, typename data_adaptor<T>::data_type>);
    ASSERT(std::is_base_of_v<data_t, data_adaptor<T>>);

    {
        std::cout << "Testing defaulted adaptor" << std::endl;
        data_adaptor<T> defaulted;
        test_namidata_default(defaulted);

        std::cout << "Testing default-assigned adaptor" << std::endl;
        data_adaptor<T> assigned = T{};
        test_namidata_default(assigned);
    }

    { data_adaptor<T> value { cvec };
        ASSERT(value.size() == 7);
        ASSERT(value.rows() == 1);
        ASSERT(value.cols() == 7);
        ASSERT(value.empty() == false);
        for (int i = 0; i < 7; i++)
        {
            ASSERT(value.at(i)    == i - 3);
            ASSERT(value.at(0, i) == i - 3);
        }
        ASSERT(value.min() == -3);
        ASSERT(value.max() ==  3);
    }
}

/*
 * cvec must have these contents:
 * [ [ -3 -2 -1 0 1 2 3 ] ]
 *
 * cmat must have these contents:
 * [ [ 11 12 13 14 ]
 *   [ 21 22 23 24 ]
 *   [ 31 32 33 34 ] ]
 */
template <Namidata T>
void test_namidata(T const& cmat, T const& cvec)
{
    test_namidata(cvec);

    { data_adaptor<T> value { cmat };
        ASSERT(value.size() == 3 * 4);
        ASSERT(value.rows() == 3);
        ASSERT(value.cols() == 4);
        ASSERT(value.empty() == false);
        for (int r = 0; r < 3; r++)
            for (int c = 0; c < 4; c++)
            {
                ASSERT(value.at(4*r + c) == 10*(r+1) + (c+1));
                ASSERT(value.at(r, c)    == 10*(r+1) + (c+1));
            }
        ASSERT(value.min() == 11);
        ASSERT(value.max() == 34);
    }

    auto share = std::make_unique<data_adaptor<T>>(cmat);
}

} // namespace test
namespace nami {

#endif // TEST

