/* Copyright 2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>


#ifdef TEST //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#undef Status
#include <opencv2/opencv.hpp>

namespace test
{

void test_openCV()
{

    std::cout << "Testing openCV " << std::endl;
    static_assert(Namidata_openCV<cv::Mat>);
    static_assert(std::semiregular<cv::Mat>);

    signed char data_m_d [] =
      { 11, 12, 13, 14
      , 21, 22, 23, 24
      , 31, 32, 33, 34  };
    cv::Mat m_d (3, 4, CV_8UC1, data_m_d);

    signed char data_v_d [] = { -3, -2, -1, 0, 1, 2, 3 };
    cv::Mat v_d (1, 7, CV_8S, data_v_d);

    test::test_namidata<cv::Mat>(m_d, v_d);
}

void test()
{
    test_openCV();
}

} // namespace test
#endif // TEST

