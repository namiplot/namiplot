/* Copyright 2020-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

/* Theme subsystem
 * Each keyword may set manually or delegated.
 * If delegated, plot->theme sets the value. The theme may be also
 * set manually or delegated, same as other keywords.
 */

//__________________________________ THEMES __________________________________||

#define NAMI_THEME_ATTR(name) \
    decltype(nami::name)::value_type name

// Global theme defaults
struct theme_t
{
    struct {
        color_t  color                     = namisand;
        unsigned width                     = 600;
        unsigned height                    = 600;
    } canvas;

    struct theme_base_chart
    {
        NAMI_THEME_ATTR(disable)           = false;
        NAMI_THEME_ATTR(name)              = "";
    }base_chart;

    struct theme_grille
    {
        NAMI_THEME_ATTR(n_cols)            = 2u;
        NAMI_THEME_ATTR(ratios_cols)       = {};
        NAMI_THEME_ATTR(ratios_rows)       = {};
    }grille;

    struct theme_base_inset
    {
        NAMI_THEME_ATTR(background)        = { 0.f, white };
        NAMI_THEME_ATTR(border_color)      = black;
        NAMI_THEME_ATTR(border_width)      = 1.;
        NAMI_THEME_ATTR(no_clip)           = false;
        NAMI_THEME_ATTR(margins)           = 25_pct;
    }base_inset;

    struct theme_inset
    {
        NAMI_THEME_ATTR(border_color)      = black;
        NAMI_THEME_ATTR(border_width)      = 1.;
        NAMI_THEME_ATTR(margins)           = 25_pct;
    }inset;

    struct theme_frame
    {
        NAMI_THEME_ATTR(title)             = "";
        NAMI_THEME_ATTR(xlabel)            = "";
        NAMI_THEME_ATTR(ylabel)            = "";
    }frame;

    struct theme_flat_chart
    {
        NAMI_THEME_ATTR(xy_views)          = {{data_min, data_max}};
    }flat_chart;

    struct theme_curve
    {
        NAMI_THEME_ATTR(color)             = { namisea, namisun, namisky };
        NAMI_THEME_ATTR(line_pattern)      = solid;
        NAMI_THEME_ATTR(line_pattern_pace) = 0ms;
        NAMI_THEME_ATTR(width)             = 1.;
    }curve;

    struct theme_area
    {
        NAMI_THEME_ATTR(fill_color)        = { lightnamisea
                                             , lightnamisun
                                             , lightnamisky
                                             };
    }area;

    struct theme_stacked_area
    {
        NAMI_THEME_ATTR(fill_to_xaxis)     = {};
    }stacked_area;

    struct theme_text
    {
        NAMI_THEME_ATTR(background)        = { 0.f, white };
        NAMI_THEME_ATTR(color)             = black;
        NAMI_THEME_ATTR(coord)             = { 50_pct, 50_pct };
        NAMI_THEME_ATTR(font_align)        = C;
        NAMI_THEME_ATTR(font_size)         = 14.;
        NAMI_THEME_ATTR(rotation)          = 0_rad;
    }text;

    struct theme_grid
    {
        NAMI_THEME_ATTR(color)             = darkgrey;
        NAMI_THEME_ATTR(line_pattern)      = dotted;
    }grid;

};

// Themes are special
DEFINE_KEYWORD(theme_t, theme);

extern const theme_t namitheme;
extern const theme_t plain;
extern const theme_t dark;

inline theme_t const& default_primary_theme = namitheme;

// Keyword_wrapper for theme_t
template <auto plot_offset, auto attr_offset>
class keyword_wrapper<decltype(nami::theme), plot_offset, attr_offset>
{
public:
    using type = theme_t;

private:
    bool  delegate_value = true;
    theme_t const* value = &namitheme;

public:

    keyword_wrapper() = default;
    keyword_wrapper(keyword_wrapper const& copy) = default;
    keyword_wrapper(theme_t const& copy)
      : delegate_value {false}
      , value {&copy}
    { }

    keyword_wrapper & operator = (keyword_wrapper const& copy) = default;
    keyword_wrapper & operator = (theme_t const& copy)
    {
        value = &copy;
        delegate_value = false;
        return *this;
    }

public:
    operator theme_t const& () const { return *value; }
    bool    delegate        () const { return delegate_value; }

    theme_t const& operator () () const {
        if (delegate_value)
            return *primary_theme;
        else
            return *value;
    }
};

