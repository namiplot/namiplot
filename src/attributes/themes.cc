/* Copyright 2020-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {

// See fixme in root.cc
//theme_t const* primary_theme = &plain;
//const theme_t namitheme {};

const theme_t plain =
{
    .canvas = {
        .color  = white,
        .width  = 800,
        .height = 400,
    },
    .base_chart = {
        .disable = false,
        .name    = ""
    },
    .grille = {},
    .base_inset = {},
    .inset = {},
    .frame = {},
    .flat_chart = {},
    .curve = {.color = blue, .width=1},
    .area = {},
    .stacked_area = {},
    .text = {},
    .grid = {},
};

const theme_t dark =
{
    .canvas = {
        .color  = darkgrey,
        .width  = 800,
        .height = 400,
    },
    .base_chart = {
        .disable = false,
        .name    = ""
    },
    .grille = {},
    .base_inset = {},
    .inset = {},
    .frame = {},
    .flat_chart = {},
    .curve = {},
    .area = {},
    .stacked_area = {},
    .text = {},
    .grid = {},
};

} // namespace nami

#ifdef TEST //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#include "../utils/test_framework.hh"
namespace test
{

void test()
{
}

} // namespace test
#endif // TEST

