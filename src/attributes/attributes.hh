/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

//_____________________________ VIEW_LIMIT_T _________________________________||


/* TODO:
 * inf_tick
 * min_tick
 * max_tick
 * sup_tick
 * loose = data_min - 0.05*canvas_span
 *       = data_max + 0.05*canvas_span
 * ticks = sup_tick
 *       = inf_tick
 * tight = data_min
 *       = data_max
 * density
 */

class view_limit_t
{
    struct coefficients
    {
        double data_min = 0.;
        double data_max = 0.;
        double canvas_span = 0.;
        //double inf_tick    = 0.; // TODO
        //double min_tick    = 0.;
        //double max_tick    = 0.;
        //double sup_tick    = 0.;
        //double density     = 0.;
        double constant = 0.;

        constexpr bool operator == (coefficients const& rhs) const = default;
    } coef;

public: /* Regular type traits */
    constexpr view_limit_t() = default;

    constexpr view_limit_t(view_limit_t const& copy) = default;

    constexpr view_limit_t & operator = (view_limit_t const& assign) = default;

    constexpr
    bool operator == (view_limit_t const& rhs) const = default;

public: /* Type specific constructors and assignments */
    constexpr view_limit_t(double coef_data_min,
                           double coef_data_max,
                           double coef_constant,
                           double coef_canvas_span)
    {
        coef.data_min    = coef_data_min;
        coef.data_max    = coef_data_max;
        coef.canvas_span = coef_canvas_span;
        coef.constant    = coef_constant;
    }

    constexpr view_limit_t(double coef_constant)
    {
        coef.constant = coef_constant;
    }

private: /* Operators */
    constexpr
    void operate_members (view_limit_t const& rhs,
                          std::invocable<double &, double> auto op)
    {
        op(coef.data_min,    rhs.coef.data_min);
        op(coef.data_max,    rhs.coef.data_max);
        op(coef.canvas_span, rhs.coef.canvas_span);
        op(coef.constant,      rhs.coef.constant);
    }

    constexpr
    void operate_members (double rhs,
                          std::invocable<double &, double> auto op)
    {
        op(coef.data_min,    rhs);
        op(coef.data_max,    rhs);
        op(coef.canvas_span, rhs);
        op(coef.constant,    rhs);
    }


public: /* Operators */
    constexpr
    view_limit_t &
    operator += (view_limit_t const& rhs)
    {
        operate_members(rhs, [](double & l, double r) constexpr { l += r; });
        return *this;
    }

    constexpr
    view_limit_t &
    operator -= (view_limit_t const& rhs)
    {
        operate_members(rhs, [](double & l, double r) constexpr { l -= r; });
        return *this;
    }

    constexpr
    view_limit_t &
    operator *= (double factor)
    {
        operate_members(factor, [](double & l, double r) constexpr { l *= r; });
        return *this;
    }

    constexpr
    view_limit_t &
    operator /= (double factor)
    {
        operate_members(factor, [](double & l, double r) constexpr { l /= r; });
        return *this;
    }

public:
    double process_value(double data_min,
                         double data_max,
                         double canvas_span_arg) const;

};

/* ***************  OPERATORS *************** */

constexpr view_limit_t
operator + (view_limit_t lhs, view_limit_t const& rhs)
{ return lhs += rhs; }

constexpr view_limit_t
operator - (view_limit_t lhs, view_limit_t const& rhs)
{ return lhs -= rhs; }

constexpr view_limit_t
operator * (view_limit_t lhs, double rhs)
{ return lhs *= rhs; }

constexpr view_limit_t
operator * (double lhs,       view_limit_t rhs)
{ return rhs *= lhs; }

constexpr view_limit_t
operator / (view_limit_t lhs, double rhs)
{ return lhs /= rhs; }

/* ******************  FACTORIES  ****************** */
// Hardcoded points
constexpr const view_limit_t data_min    = { 1,   0,   0, 0};
constexpr const view_limit_t data_max    = { 0,   1,   0, 0};
constexpr const view_limit_t canvas_span = { 0,   0,   1, 0};
// Presets
constexpr const view_limit_t data_span   = {-1,   1,   0, 0};
constexpr const view_limit_t data_middle = { 0.5, 0.5, 0, 0};


//________________________________ VIEW_T ____________________________________||

// enum in struct impedes implicit conversion of enum to integer.
struct view_auto_t
{
    enum { /* TODO: ticks, */ loose, tight, } value;

    constexpr auto operator <=> (view_auto_t const&) const = default;
};
constexpr view_auto_t loose {view_auto_t::loose};
constexpr view_auto_t tight {view_auto_t::tight};

class flat_chart;

class view_t
{
public:
    // If view_t, then it becomes square.
    // If min and max view_t, then it becomes square and centered on data.
    view_limit_t lower = data_min;
    view_limit_t upper = data_max;

public:
    constexpr view_t() = default;

    // TODO: If lower_upper is type double, then lower_upper refers to the
    // width of the view in data space centered in its mid-point.
    constexpr view_t (view_auto_t auto_spec)
    {
        if (auto_spec == loose)
        {
            lower -= data_span * 0.05;
            upper += data_span * 0.05;
        }
        else if (auto_spec == tight)
            {}
        else
            throw;
    }

    constexpr view_t(view_limit_t const& lower_arg,
                     view_limit_t const& upper_arg)
      : lower(lower_arg)
      , upper(upper_arg)
    { }

    constexpr bool operator == (view_t const&) const = default;

public:
    enum axis_t { X, Y };

    std::pair<double,double>
    view_limits(
            draw_context const& cntxt,
            flat_chart const& data_plot,
            axis_t axis
            ) const;

};


//_________________________________ FLAT_VIEW ________________________________||

struct flat_view
{
    view_t x;
    view_t y;

    constexpr flat_view() = default;

    constexpr flat_view(view_t const& xy)
      : x {xy}
      , y {xy}
    {}

    constexpr flat_view(view_limit_t const& lower, view_limit_t const& upper)
      : x {lower, upper}
      , y {lower, upper}
    {}


    constexpr flat_view(view_t const& x_arg, view_t const& y_arg)
      : x {x_arg}
      , y {y_arg}
    {}

    constexpr bool operator == (flat_view const&) const = default;
};


//__________________________________ ALIGN ___________________________________||


enum text_align_t
{
    NW,  // Relative to font dimensions.
    N,
    NE,
    W,
    C,
    E,
    SW,
    S,
    SE,

    BBOX_NW,  // Relative to the rendered text bounding box.
    BBOX_N,
    BBOX_NE,
    BBOX_W,
    BBOX_C,
    BBOX_E,
    BBOX_SW,
    BBOX_S,
    BBOX_SE,
};


//__________________________________ COLOR ___________________________________||


// namiplot color scheme
constexpr uint32_t namiblack      = 0x3A3738;
constexpr uint32_t namisky        = 0x2B83BC;
constexpr uint32_t namisea        = 0x53C4D0;
constexpr uint32_t namisand       = 0xFBFEEE;
constexpr uint32_t namisun        = 0xF7B632;
constexpr uint32_t lightnamiblack = 0xC3BFBF;
constexpr uint32_t lightnamisky   = 0xC1E1F7;
constexpr uint32_t lightnamisea   = 0xC9F0F5;
constexpr uint32_t lightnamisand  = 0xFDFEFA;
constexpr uint32_t lightnamisun   = 0xFEF1D7;

// RGB
constexpr uint32_t lightred       = 0xFFA0A0;
constexpr uint32_t lightgreen     = 0xA0FFA0;
constexpr uint32_t lightblue      = 0xA0A0FF;
constexpr uint32_t red            = 0xFF0000;
constexpr uint32_t green          = 0x00FF00;
constexpr uint32_t blue           = 0x0000FF;
constexpr uint32_t darkred        = 0xA00000;
constexpr uint32_t darkgreen      = 0x00A000;
constexpr uint32_t darkblue       = 0x0000A0;

// CMY
constexpr uint32_t lightcyan      = 0xA0FFFF;
constexpr uint32_t lightmagenta   = 0xFFA0FF;
constexpr uint32_t lightyellow    = 0xFFFFA0;
constexpr uint32_t cyan           = 0x00FFFF;
constexpr uint32_t magenta        = 0xFF00FF;
constexpr uint32_t yellow         = 0xFFFF00;
constexpr uint32_t darkcyan       = 0x00A0A0;
constexpr uint32_t darkmagenta    = 0xA000A0;
constexpr uint32_t darkyellow     = 0xA0A000;

// B/W
constexpr uint32_t white          = 0xFFFFFF;
constexpr uint32_t lightgrey      = 0xC0C0C0;
constexpr uint32_t grey           = 0x808080;
constexpr uint32_t darkgrey       = 0x404040;
constexpr uint32_t black          = 0x000000;

// Others
// Also: transparent (defined below)
constexpr uint32_t aqua           = 0x7FFFD4;
constexpr uint32_t beige          = 0xF5F5DC;
constexpr uint32_t brown          = 0x964B00;
constexpr uint32_t lime           = 0xBFFF00;
constexpr uint32_t maroon         = 0x800000;
constexpr uint32_t navy           = 0x000080;
constexpr uint32_t olive          = 0x808000;
constexpr uint32_t orange         = 0xFFA500;
constexpr uint32_t peach          = 0xFFDAB9;
constexpr uint32_t pink           = 0xFFC0CB;
constexpr uint32_t purple         = 0x800080;
constexpr uint32_t silver         = 0xC0C0C0;
constexpr uint32_t snow           = 0xF8F8FF;
constexpr uint32_t teal           = 0x008080;
constexpr uint32_t violet         = 0xA020F0;

// Matlab colors
constexpr uint32_t matblue        = 0x0072BD;
constexpr uint32_t matorange      = 0xD95319;
constexpr uint32_t matyellow      = 0xEDB120;
constexpr uint32_t matpurple      = 0x7E2F8E;
constexpr uint32_t matgreen       = 0x77AC30;
constexpr uint32_t matcyan        = 0x4DBEEE;
constexpr uint32_t matred         = 0xA2142F;
constexpr uint32_t matgrey        = 0xF0F0F0;


class color_t
{
public:
    /* Red, Green, Blue and Alpha */
    float a,r,g,b;

    constexpr color_t()                          : color_t(1, 0, 0, 0) { }
    constexpr color_t(float R, float G, float B) : color_t(1, R, G, B) { }

    // Force alpha channel over the ARGB value.
    constexpr color_t(float A, uint32_t hex_ARGB)
      : a { std::clamp(A, 0.f, 1.f) }
      , r { ((hex_ARGB >> 16) & 0xFF) / 255.f }
      , g { ((hex_ARGB >>  8) & 0xFF) / 255.f }
      , b { ((hex_ARGB >>  0) & 0xFF) / 255.f }
    { }
    constexpr color_t(float A, color_t const& RGB)
      : color_t(RGB)
    { a = A; }

    // If A channel is 0, force to 1.
    // Use color_t(A, hex_ARGB) to use a 100% transparent color.
    constexpr color_t(uint32_t hex_ARGB)
      : a { ((hex_ARGB >> 24) & 0xFF) / 255.f }
      , r { ((hex_ARGB >> 16) & 0xFF) / 255.f }
      , g { ((hex_ARGB >>  8) & 0xFF) / 255.f }
      , b { ((hex_ARGB >>  0) & 0xFF) / 255.f }
    { if (a == 0.f) a = 1.f; }

    constexpr color_t(float A, float R, float G, float B)
      : a { std::clamp(A, 0.f, 1.f) }
      , r { std::clamp(R, 0.f, 1.f) }
      , g { std::clamp(G, 0.f, 1.f) }
      , b { std::clamp(B, 0.f, 1.f) }
    { }

    friend constexpr
    bool operator == (color_t const& lhs, color_t const& rhs)
    {
        return lhs.r == rhs.r
            && lhs.g == rhs.g
            && lhs.b == rhs.b
            && lhs.a == rhs.a;
    }
};


constexpr
bool operator != (color_t const& lhs, color_t const& rhs)
{ return !(lhs == rhs); }


// Special color
constexpr color_t transparent   = {0.f, 0x000000};


/* ***************  CAIRO OVERLOADS *************** */

inline void
cairo_set_source_rgb(cairo_t *cr, color_t const& color)
{
    if (color.a > 0.)
        cairo_set_source_rgb(cr, color.r, color.g, color.b);
}
inline void
cairo_set_source_rgba(cairo_t *cr, color_t const& color)
{
    cairo_set_source_rgba(cr, color.r, color.g, color.b, color.a);
}



//________________________________ LINE_DASH _________________________________||


using line_pattern_t = std::vector<double>;

const line_pattern_t solid   = { };
const line_pattern_t dashed  = {9, 6};
const line_pattern_t dotted  = {1, 3};
const line_pattern_t dashdot = {6, 3, 1, 3};

void
set_dash_in_cairo(draw_context const& cntxt,
                  line_pattern_t patt,
                  double line_width,
                  std::chrono::milliseconds pace);

//_______________________________ LATEX_STYLE ________________________________||


enum math_style_t {
    inlinemath,
    displaymath,
};

//______________________________ SCATTER SYMBOLS _____________________________||

enum symbol_preset_t {
    plus     = 0,
    circle   = 1,
    asterisk = 2,
    point    = 3,
    cross    = 4,
    square   = 5,
    diamond  = 6,
    /*
    tri_up,
    tri_down,
    tri_left,
    tri_right,
    star5,
    star6,
    hexagon,
    clover,
    heart,
    // TODO: See matplotlib.markers for many more imprint presets.
    */
};

