/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami::literals {

//_____________________________ CANVAS_HALF_POINT ____________________________||

double
canvas_half_point:: value(unsigned canvas_span_arg) const
{
    double ret = 0;
    ret += pixels;
    ret += perunit * canvas_span_arg;
    return ret;
}

//_______________________________ CANVAS_POINT _______________________________||

double
norm(draw_context const& cntxt, canvas_point const& pt)
{
    double X = pt.x.value(cntxt.width);
    double Y = pt.y.value(cntxt.height);
    return sqrt( X*X + Y*Y );
}

} // namespace nami::literals

#ifdef TEST //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace test
{

bool comp (double lhs, double rhs)
{
    if (   lhs > rhs - rotation_t::absolute_compare_tolerance_rad
        && lhs < rhs + rotation_t::absolute_compare_tolerance_rad )
          return true;
      else
          return false;
}
void test_rotation_t()
{
    const double pi = std::numbers::pi;
    std::cout << "DEBUG: Testing rotation_t..." << std::endl;
    test_regular<rotation_t>(1_rad, false);

    ASSERT( comp(deg(-361).deg(),   -1) );
    ASSERT( comp(deg(-360).deg(),  -0.) );
    ASSERT( comp(deg(-359).deg(),    1) );
    ASSERT( comp(deg(-271).deg(),   89) );
    ASSERT( comp(deg(-270).deg(),   90) );
    ASSERT( comp(deg(-269).deg(),   91) );
    ASSERT( comp(deg(-181).deg(),  179) );
    ASSERT( comp(deg(-180).deg(), -180) );
    ASSERT( comp(deg(-179).deg(), -179) );
    ASSERT( comp(deg( -91).deg(),  -91) );
    ASSERT( comp(deg( -90).deg(),  -90) );
    ASSERT( comp(deg( -89).deg(),  -89) );
    ASSERT( comp(deg(  -1).deg(),   -1) );
    ASSERT( comp(deg(   0).deg(),   0.) );
    ASSERT( comp(deg(   1).deg(),    1) );
    ASSERT( comp(deg(  89).deg(),   89) );
    ASSERT( comp(deg(  90).deg(),   90) );
    ASSERT( comp(deg(  91).deg(),   91) );
    ASSERT( comp(deg( 179).deg(),  179) );
    ASSERT( comp(deg( 180).deg(),  180) );
    ASSERT( comp(deg( 181).deg(), -179) );
    ASSERT( comp(deg( 269).deg(),  -91) );
    ASSERT( comp(deg( 270).deg(),  -90) );
    ASSERT( comp(deg( 271).deg(),  -89) );
    ASSERT( comp(deg( 359).deg(),   -1) );
    ASSERT( comp(deg( 360).deg(),   0.) );
    ASSERT( comp(deg( 361).deg(),    1) );

    ASSERT( !comp(deg(   0).deg(),    1) );
    ASSERT( !comp(deg(   0).deg(),   -1) );
    ASSERT( !comp(deg( 180).deg(),  179) );
    ASSERT( !comp(deg( 180).deg(), -179) );
    ASSERT( !comp(deg(-180).deg(),  179) );
    ASSERT( !comp(deg(-180).deg(), -179) );

    ASSERT( deg(-361) == deg(  -1) );
    ASSERT( deg(-360) == deg( -0.) );
    ASSERT( deg(-359) == deg(   1) );
    ASSERT( deg(-271) == deg(  89) );
    ASSERT( deg(-270) == deg(  90) );
    ASSERT( deg(-269) == deg(  91) );
    ASSERT( deg(-181) == deg( 179) );
    ASSERT( deg(-180) == deg(-180) );
    ASSERT( deg(-179) == deg(-179) );
    ASSERT( deg( -91) == deg( -91) );
    ASSERT( deg( -90) == deg( -90) );
    ASSERT( deg( -89) == deg( -89) );
    ASSERT( deg(  -1) == deg(  -1) );
    ASSERT( deg(   0) == deg(  0.) );
    ASSERT( deg(   1) == deg(   1) );
    ASSERT( deg(  89) == deg(  89) );
    ASSERT( deg(  90) == deg(  90) );
    ASSERT( deg(  91) == deg(  91) );
    ASSERT( deg( 179) == deg( 179) );
    ASSERT( deg( 180) == deg( 180) );
    ASSERT( deg( 181) == deg(-179) );
    ASSERT( deg( 269) == deg( -91) );
    ASSERT( deg( 270) == deg( -90) );
    ASSERT( deg( 271) == deg( -89) );
    ASSERT( deg( 359) == deg(  -1) );
    ASSERT( deg( 360) == deg(  0.) );
    ASSERT( deg( 361) == deg(   1) );

    ASSERT( deg(   0) != deg(   1) );
    ASSERT( deg(   0) != deg(  -1) );
    ASSERT( deg( 180) != deg( 179) );
    ASSERT( deg( 180) != deg(-179) );
    ASSERT( deg(-180) != deg( 179) );
    ASSERT( deg(-180) != deg(-179) );


    rotation_t th0;
    ASSERT(rad(pi/6) == deg(30));
    ASSERT(deg(720) == rotation_t {} );
}

void test()
{
    using namespace nami;
    draw_context drctxt {nullptr, 10, 10, {}};

    // norm
    canvas_point pt = {3_px, 4_px};
    ASSERT(norm(drctxt, pt) == 5);

    test_rotation_t();

    // TODO Pending all other tests
}

} // namespace test

#endif // TEST

