/* Copyright 2020-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "../utils/test_framework.hh"
#include "keyword.hh"

#ifdef TEST
namespace test {

void test_keywords()
{
//TODO: test_keywords
// Check the order of parameters in the constructor of a class with keywords.
// Check that compilation of the constructor of a class with keywords fails if
// the class does not accept those keywords.
}

void test()
{
    test_keywords();
}

} // namespace test
#endif // TEST

