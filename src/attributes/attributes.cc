/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {


//_____________________________ VIEW_LIMIT_T _________________________________||

double
view_limit_t:: process_value(double data_min_arg,
                             double data_max_arg,
                             double canvas_span_arg) const
{
    double ret = 0.;

    ret += coef.data_min * data_min_arg;
    ret += coef.data_max * data_max_arg;
    ret += coef.canvas_span * canvas_span_arg;
    ret += coef.constant;

    return ret;
}


#ifdef TEST
} // namespace nami
namespace test {

void test_view_limit_t()
{
    std::cout << "Testing view_limit_t..." << std::endl;
    test_regular<view_limit_t>(view_limit_t(1, 0, 0, 0), false);
}

} // namespace test
namespace nami {
#endif

//________________________________ VIEW_T ____________________________________||


//double
//view_t:: pixel_to_data (double y) const
//{
    //if (a != 0.)
        //return [> x= <] (y - b)/a;
    //else
        //return 0.;
//}
    //a = canvas_span / (upp - low);
    //b = -a * low;

std::pair<double,double>
view_t:: view_limits(
            draw_context const& cntxt,
            flat_chart const& data_plot,
            axis_t axis
            ) const
{
    data_cptr const& data = (axis == X ? data_plot.x : data_plot.y);
    unsigned span = (axis == X ? cntxt.width : cntxt.height);

    double low, upp;
    low = lower.process_value(data->min(), data->max(), span);
    upp = upper.process_value(data->min(), data->max(), span);
    return {low, upp};
}


//_________________________________ FLAT_VIEW ________________________________||

#ifdef TEST
} // namespace nami
namespace test {

void test_flat_view()
{
    std::cout << "Testing flat_view..." << std::endl;

    test_regular<flat_view>(flat_view{loose}, false);

    ASSERT(flat_view{}      == flat_view{tight});
    ASSERT(flat_view{loose} == flat_view(loose, loose));
    ASSERT(flat_view{tight} == flat_view{data_min, data_max});
    ASSERT(flat_view{tight} == flat_view{tight, {data_min, data_max}});
    ASSERT(flat_view{tight} != flat_view{tight, {data_min, data_max + 1}});
    ASSERT(flat_view{loose} != flat_view{tight, {data_min, data_max}});

    ASSERT(flat_view{0,1}   == flat_view{{0,1},{0,1}});
    ASSERT(flat_view{0,1}   != flat_view{{0,1},{1,2}});
    ASSERT(flat_view{0,1}   != flat_view{{1,2},{0,1}});
}

} // namespace test
namespace nami {
#endif


//________________________________ LINE_DASH _________________________________||

void
set_dash_in_cairo(draw_context const& cntxt,
                  line_pattern_t patt,
                  double line_width,
                  std::chrono::milliseconds pace)
{
    if (patt.size() >= 2)
    {
        std::for_each(patt.begin(), patt.end(),
                [line_width](double & p){ p *= line_width; } );

        double dash_speed = pace.count() / 1000. *
            std::accumulate(patt.begin(), patt.end(), 0);

        cairo_set_dash(cntxt, patt.data(), patt.size(),
	    -(dash_speed * cntxt.anim.frame_count / cntxt.anim.framerate) );
    }
}


} // namespace nami

#ifdef TEST
namespace test {

void test()
{
    test_view_limit_t();
    test_flat_view();
}

} // namespace test
#endif

