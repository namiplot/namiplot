/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

//_________________________________ CONCEPTS _________________________________||

// Utility class that tags assigned keywords for concept Assigned_keyword
class assigned_keyword_base_t
{ };

// The keyword argument has been assigned to a value
template <typename AP>
concept Assigned_keyword =
    std::is_base_of<assigned_keyword_base_t, AP>::value &&
    std::is_constructible<AP, typename AP::value_type>::value &&
    requires (AP a) {
        typename AP::value_type;
        a.value;
    };

//___________________________ ASSIGNED KEYWORDS ______________________________||


template <int unique_type_id, typename T>
struct assigned_keyword_t : assigned_keyword_base_t
{
    using value_type = T;
    static constexpr int id = unique_type_id;

    T value;

    assigned_keyword_t (T const& value_arg)
        : value(value_arg)
    { }
};

// Since flags do not need assignment, the unassigned and assigned
// classes must be the same.
template <int unique_type_id>
struct flag_keyword_t : assigned_keyword_base_t
{
    using value_type    = bool;
    using assigned_type = flag_keyword_t<unique_type_id>;
    static constexpr int id = unique_type_id;

    // The unassigned value replaces false with true by default.
    bool value = true;


    // The trivial constructor, copy and assignment remove a GCC warning.
    flag_keyword_t () = default;

    explicit flag_keyword_t (bool value_arg)
        : value(value_arg)
    { }

    assigned_type operator ! () const
    { return flag_keyword_t{false}; }

    assigned_type operator = (bool val) const
    { return flag_keyword_t{val}; }
};

//__________________________ UNASSIGNED PARAMETERS ___________________________||


template <int unique_type_id, typename T>
struct unassigned_keyword_t
{
    using value_type    = T;
    using assigned_type = assigned_keyword_t<unique_type_id, T>;
    static constexpr int id = unique_type_id;


    assigned_type operator = (value_type const& val) const
    { return val; }

    assigned_type operator = (auto const& val) const
    { return value_type{val}; }
};


// This template specialization removes the narrowing warning from int
// to double.
template <int unique_type_id>
struct unassigned_keyword_t<unique_type_id, double>
{
    using value_type    = double;
    using assigned_type = assigned_keyword_t<unique_type_id, double>;
    static constexpr int id = unique_type_id;


    assigned_type operator = (double val) const
    { return val; }

    assigned_type operator = (int val) const
    { return static_cast<double>(val); }
};


// TODO Use string literals, i.e. #NAME, instead of (int)__COUNTER__.
#define DEFINE_KEYWORD(TYPE, NAME) \
    inline unassigned_keyword_t<__LINE__, TYPE> NAME

#define DEFINE_BOOLEAN_KEYWORD(NAME) \
    inline flag_keyword_t<__LINE__> NAME

template <typename Type>
concept UnassignedKeyword =
    std::same_as< std::remove_const_t<decltype(Type::id)>, int >
    && requires {
        typename Type::value_type;
        typename Type::assigned_type;
    };

//_________________________________ THEMES ___________________________________||

struct delegate_t {} inline delegate;
struct theme_t;
extern theme_t const* primary_theme;

//___________________________ STANDARD ATTRIBUTES ____________________________||

// Keyword_wrapper for classes, except theme_t
// TODO: operator .* not overloaded: Direct member access to wrapped type
// is not possible without bypassing theme. Reconsider it when reflection
// becomes available.
template <UnassignedKeyword uattr, auto plot_offset, auto attr_offset>
class keyword_wrapper
{
public:
    using type = uattr::value_type;

private:
    bool delegate_value = true;
    type value {};

public:
    /** Constructors **/
    keyword_wrapper() = default;

    keyword_wrapper(keyword_wrapper const& copy) = default;
    keyword_wrapper & operator = (keyword_wrapper const& copy) = default;

    keyword_wrapper(delegate_t) { }
    keyword_wrapper & operator = (delegate_t) {
        value = {};
        delegate_value = true;
        return *this;
    }

    template <typename ... Attrs>
    explicit keyword_wrapper(Attrs ... attrs)
      : delegate_value { false }
      , value { std::forward(attrs)... }
    { }
    keyword_wrapper & operator = (type const& copy) {
        value = copy;
        delegate_value = false;
        return *this;
    }


public:
    operator type const& () const { return operator (); }
    bool delegate        () const { return delegate_value; }

    type const& operator () () const {
        if (delegate_value)
            return primary_theme->*plot_offset.*attr_offset;
        else
            return value;
    }

};

// Keyword_wrapper for basic types
// If type does not support an operation (e.g. float s; s <<= 1;),
// it will not compile. Hence this class can be used for all basic types.
template <UnassignedKeyword uattr, auto plot_offset, auto attr_offset>
    requires std::is_arithmetic_v<typename uattr::value_type>
          || std::is_enum_v<typename uattr::value_type>
class keyword_wrapper<uattr, plot_offset, attr_offset>
{
public:
    using type = uattr::value_type;

private:
    bool delegate_value = true;
    type value {};

public:
    keyword_wrapper() = default;

    keyword_wrapper(type copy) {
        value = copy;
        delegate_value = false;
    }
    keyword_wrapper & operator = (type copy) {
        value = copy;
        delegate_value = false;
        return *this;
    }

    keyword_wrapper(delegate_t) : keyword_wrapper() {}
    keyword_wrapper & operator = (delegate_t) {
        value = { };
        delegate_value = true;
        return *this;
    }

public:
#define NAMI_WRAPPER_OP( OP ) \
    template <typename Vtype> keyword_wrapper & operator OP##=  (Vtype val) \
    { value = operator () OP val; delegate_value = false; return *this; }

    NAMI_WRAPPER_OP(+)
    NAMI_WRAPPER_OP(-)
    NAMI_WRAPPER_OP(*)
    NAMI_WRAPPER_OP(/)
    NAMI_WRAPPER_OP(%)
    NAMI_WRAPPER_OP(&)
    NAMI_WRAPPER_OP(|)
    NAMI_WRAPPER_OP(^)
    NAMI_WRAPPER_OP(<<)
    NAMI_WRAPPER_OP(>>)

    type operator +() const { return + operator (); }
    type operator -() const { return - operator (); }
    type operator ~() const { return ~ operator (); }

#undef NAMI_WRAPPER_OP

public:
    operator type () const { return operator (); }
    bool delegate () const { return delegate_value; }

    type operator () () const {
        if (delegate_value)
            return primary_theme->*plot_offset.*attr_offset;
        else
            return value;
    }
};

#define DEFINE_ATTRIBUTE(PLOT, ATTR) \
protected: \
    void attribute_initialization( \
            decltype(nami::ATTR)::assigned_type const& keyword) \
    { ATTR = keyword.value; } \
public: \
    keyword_wrapper<decltype(nami::ATTR), \
        &theme_t::PLOT, \
        &theme_t::theme_##PLOT::ATTR \
    > ATTR \

