/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

class data_t;
struct draw_context;

// TODO: Test the inlining of the namespace literals
inline namespace literals
{

//___________________________________ Hz _____________________________________||


using namespace std::literals::chrono_literals;

constexpr
std::chrono::milliseconds
operator "" _Hz  (unsigned long long int hz)
{
    return std::chrono::milliseconds{1'000'000/hz};
}

constexpr
std::chrono::microseconds
operator "" _Hz  (long double hz)
{
    return std::chrono::microseconds{static_cast<long int>(1.e6/hz)};
}


//_____________________________ CANVAS_HALF_POINT ____________________________||



// Screen coordinate.
class canvas_half_point
{
private: /* Constructors */
    enum coordinate_type { PIXEL, PERUNIT };
    constexpr canvas_half_point(double value, coordinate_type t)
    {
        switch (t)
        {
        case PIXEL:
            pixels = value;
            break;
        case PERUNIT:
            perunit = value;
            break;
        default:
            throw "canvas_half_point(): Unknown coordinate type";
        }
    }

    friend constexpr canvas_half_point px  (double);
    friend constexpr canvas_half_point pu  (double);
    friend constexpr canvas_half_point pct (double);

public: /* Constructors */
    constexpr canvas_half_point() = default;

public: /* Operators */

    constexpr
    canvas_half_point &
    operator += (canvas_half_point const& s2)
    {
        pixels  += s2.pixels;
        perunit += s2.perunit;
        return *this;
    }

    constexpr
    canvas_half_point &
    operator -= (canvas_half_point const& s2)
    {
        pixels  -= s2.pixels;
        perunit -= s2.perunit;
        return *this;
    }

    // Unary negation (but not substraction!) mirrors the coordinate
    // along the center of the canvas.
    friend constexpr
    canvas_half_point
    operator - (canvas_half_point s1)
    {
        s1.pixels  = -s1.pixels;
        s1.perunit = 1. - s1.perunit;
        return s1;
    }

    constexpr
    canvas_half_point &
    operator *= (double factor)
    {
        pixels  *= factor;
        perunit *= factor;
        return *this;
    }


    constexpr friend
    bool
    operator == (canvas_half_point const& lhs, canvas_half_point const& rhs)
    {
        return lhs.pixels == rhs.pixels
            && lhs.perunit == rhs.perunit;
    }

    constexpr friend
    bool
    operator != (canvas_half_point const& lhs, canvas_half_point const& rhs)
    {
        return !(lhs == rhs);
    }


public: /* Public members */
    double pixels     = 0;
    double perunit    = 0;

    /* *** Accessors *** */

    double value(unsigned canvas_span_arg) const;
};

/****************  OPERATORS  ****************/

constexpr canvas_half_point operator + (canvas_half_point s1,
                                        canvas_half_point const& s2)
{ return s1 += s2; }

constexpr canvas_half_point operator - (canvas_half_point s1,
                                        canvas_half_point const& s2)
{ return s1 -= s2; }

constexpr canvas_half_point operator * (canvas_half_point s1, double factor)
{ return s1 *= factor; }

constexpr canvas_half_point operator * (double factor, canvas_half_point s2)
{ return s2 *= factor; }


/******************  FACTORIES  ******************/

constexpr
canvas_half_point px(double value)
{
    return canvas_half_point{value, canvas_half_point::PIXEL};
}

constexpr
canvas_half_point pu(double value)
{
    return canvas_half_point{value, canvas_half_point::PERUNIT};
}

constexpr
canvas_half_point pct(double value)
{
    return canvas_half_point{value/100., canvas_half_point::PERUNIT};
}


/****************  LITERALS  ****************/

constexpr
canvas_half_point
operator""_pct (long double value)
{
    return pct(value);
}

constexpr
canvas_half_point
operator""_pct (unsigned long long int value)
{
    return pct(static_cast<double>(value));
}


constexpr
canvas_half_point
operator""_pu (long double value)
{
    return pu(value);
}

constexpr
canvas_half_point
operator""_pu (unsigned long long int value)
{
    return pu(static_cast<double>(value));
}


constexpr
canvas_half_point
operator""_px (long double value)
{
    return px(value);
}

constexpr
canvas_half_point
operator""_px (unsigned long long int value)
{
    return px(static_cast<double>(value));
}



//____________________________ CANVAS_POINT __________________________________||

class canvas_point
{
public:
    canvas_half_point x, y;

public:
    constexpr
    canvas_point() = default;

    constexpr
    canvas_point(canvas_half_point const& x_arg,
                 canvas_half_point const& y_arg)
      : x(x_arg), y(y_arg)
    { }

    /* *** Operators *** */

    constexpr
    canvas_point &
    operator += (canvas_point const& s2)
    {
        x += s2.x;
        y += s2.y;
        return *this;
    }

    friend constexpr
    canvas_point
    operator - (canvas_point s1)
    {
        s1.x = -s1.x;
        s1.y = -s1.y;
        return s1;
    }

    constexpr
    canvas_point
    operator *=(double factor)
    {
        x *= factor;
        y *= factor;
        return *this;
    }
};

constexpr
canvas_point &
operator -=(canvas_point & s1, canvas_point const& s2)
{ return s1 += -s2; }

constexpr
canvas_point
operator +(canvas_point s1, canvas_point const& s2)
{ return s1 += s2; }

constexpr
canvas_point
operator -(canvas_point s1, canvas_point const& s2)
{ return s1 += -s2; }

constexpr
canvas_point
operator *(canvas_point s1, double factor)
{ return s1 *= factor; }

constexpr
canvas_point
operator *(double factor, canvas_point s2)
{ return s2 *= factor; }


/* ***************  UTILITIES  *************** */

double norm(draw_context const& cntxt, canvas_point const& coord);


//______________________________  ROTATION ___________________________________||


class rotation_t;
constexpr rotation_t deg(double value);
/* TODO: Check user-defined conversion which is conditionally explicit
 * Then, check if multiplication by a constant, but not multiplication
 * by a rotation_t automatically converted to double is possible.
 * Also, change pi to become a rotation type so that pi/2 gives a rotation_t.
 */
// A double that can only be constructed from radians or degrees.
class rotation_t
{
private:
    double value_mod = 0;

    constexpr rotation_t & set_value(double angle)
    {
        value_mod = mod2pi(angle);
        return *this;
    }

    static constexpr double mod2pi(double angle)
    {
        return std::remainder(angle, 2*std::numbers::pi);
    }

    constexpr rotation_t(double angle)
      : value_mod(mod2pi(angle))
    { }


public:
    constexpr rotation_t() = default;

    friend constexpr rotation_t rad(double value);
    friend constexpr rotation_t deg(double value);

    // Returns -pi to pi.
    constexpr double rad() const {return value_mod;}
    constexpr double deg() const {return rad() * (360/(2*std::numbers::pi));}

    constexpr bool
    operator == (rotation_t const& rhs) const
    {
        auto tpi = [](double d){ return mod2pi(d + literals::deg(180).rad()); };
        double const& tol = absolute_compare_tolerance_rad;

        if ( -std::numbers::pi < value_mod && value_mod < std::numbers::pi)
            if (   value_mod > rhs.value_mod - tol
                && value_mod < rhs.value_mod + tol )
                return true;
            else
                return false;
        else
            if (   tpi(value_mod) > tpi(rhs.value_mod) - tol
                && tpi(value_mod) < tpi(rhs.value_mod) + tol )
                return true;
            else
                return false;
    }


    constexpr rotation_t   operator -  () const
    { return rotation_t(-value_mod); }

    constexpr rotation_t & operator += (rotation_t rhs)
    { return set_value(value_mod + rhs.value_mod); }

    constexpr rotation_t & operator -= (rotation_t rhs)
    { return set_value(value_mod - rhs.value_mod); }

    constexpr rotation_t & operator *= (double     rhs)
    { return set_value(value_mod * rhs); }

    constexpr rotation_t & operator /= (double     rhs)
    { return set_value(value_mod / rhs); }

    static constexpr double absolute_compare_tolerance_rad = 1.e-6;
};

constexpr rotation_t operator + (rotation_t lhs, rotation_t const& rhs)
{ return lhs += rhs; }

constexpr rotation_t operator - (rotation_t lhs, rotation_t const& rhs)
{ return lhs -= rhs; }

constexpr rotation_t operator * (rotation_t lhs, double            rhs)
{ return lhs *= rhs; }

constexpr rotation_t operator * (double     lhs, rotation_t        rhs)
{ return rhs *= lhs; }

constexpr rotation_t operator / (rotation_t lhs, double            rhs)
{ return lhs /= rhs; }



constexpr rotation_t rad(double value)
{ return {value}; }
constexpr rotation_t deg(double value)
{ return {value * (2.*std::numbers::pi / 360.)}; }

constexpr rotation_t operator "" _rad (long double value)
{ return rad(value); }
constexpr rotation_t operator "" _deg (long double value)
{ return deg(value); }

constexpr rotation_t operator "" _rad (unsigned long long int value)
{ return rad(value); }
constexpr rotation_t operator "" _deg (unsigned long long int value)
{ return deg(value); }




} // namespace literals


