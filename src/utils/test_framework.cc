/* Copyright 2020-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string_view>
#include <cmath>
#include "test_framework.hh"

namespace test
{

int n_errors = 0;
bool verbose = false;

void test();

std::string
fill_spaces(unsigned num)
{
    unsigned digits = log10(num) + 1;
    return std::string(5 - digits, ' ');
}

bool
operator == (std::vector<double> const& lhs, std::vector<double> const& rhs)
{
    auto lit = lhs.begin();
    auto rit = rhs.begin();
    while (lit != lhs.end() || rit != rhs.end())
    {
        if (std::nextafter(*lit, *rit) != *rit)
            return false;

        ++lit;
        ++rit;
    }
    return (lit == lhs.end() && rit == rhs.end());
}


void parse_args(int argc, char * argv[])
{
    for (int i = 1; i < argc; i++)
        if (std::string_view(argv[i]) == "-v")
            verbose = true;
}

} // namespace test

int main(int argc, char * argv[])
{
    test::parse_args(argc, argv);

    try {
        test::test();
    } catch (...) {
        std::cout << " ** TEST PROGRAM CRASHED ** with " << test::n_errors
                  << " failed asserts **" << std::endl;
        throw;
    }

    if (test::n_errors)
        std::cout << "There were " << test::n_errors
                  << " failed asserts" << std::endl;
    else
        std::cout << "All tests passed gracefully" << std::endl;

    return test::n_errors;
}
