/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace cairo { class group_guard; }

//____________________________ CAIRO_H OVERLOADS _____________________________||

inline void
cairo_move_to(draw_context      const& cntxt,
              canvas_half_point const& x,
              canvas_half_point const& y)
{
    ::cairo_move_to(cntxt,
                  x.value(cntxt.width),
                  y.value(cntxt.height) );
}

inline void
cairo_move_to(draw_context      const& cntxt,
              double                   x,
              canvas_half_point const& y)
{
    ::cairo_move_to(cntxt, x, y.value(cntxt.height));
}

inline void
cairo_move_to(draw_context      const& cntxt,
              canvas_half_point const& x,
              double                   y)
{
    ::cairo_move_to(cntxt, x.value(cntxt.width), y);
}

inline void
cairo_move_to(draw_context const& cntxt, canvas_point const& point)
{
    ::cairo_move_to(cntxt,
                  point.x.value(cntxt.width),
                  point.y.value(cntxt.height) );
}



inline void
cairo_line_to(draw_context      const& cntxt,
              canvas_half_point const& x,
              canvas_half_point const& y)
{
    ::cairo_line_to(cntxt,
                  x.value(cntxt.width),
                  y.value(cntxt.height) );
}

inline void
cairo_line_to(draw_context      const& cntxt,
              double                   x,
              canvas_half_point const& y)
{
    ::cairo_line_to(cntxt, x, y.value(cntxt.height));
}

inline void
cairo_line_to(draw_context      const& cntxt,
              canvas_half_point const& x,
              double                   y)
{
    ::cairo_line_to(cntxt, x.value(cntxt.width), y);
}

inline void
cairo_line_to(draw_context const& cntxt, canvas_point const& point)
{
    ::cairo_line_to(
            cntxt,
            point.x.value(cntxt.width),
            point.y.value(cntxt.height) );
}


//__________________________ CAIRO TRANSFORMATIONS ___________________________||

// Translations
inline draw_context const&
operator += (draw_context const& cntxt, canvas_point loc)
{
    cairo_translate(cntxt, loc.x.value(cntxt.width), loc.y.value(cntxt.height));
    return cntxt;
}

inline draw_context const&
operator -= (draw_context const& cntxt, canvas_point loc)
{
    return cntxt += -loc;
}

// Rotations
inline void
cairo_rotate(cairo_t * cr, rotation_t rot)
{
    cairo_rotate(cr, rot.rad());
}

inline void
cairo_matrix_rotate(cairo_matrix_t * matrix, rotation_t rot)
{
    cairo_matrix_rotate(matrix, rot.rad());
}

inline draw_context const&
operator += (draw_context const& cntxt, rotation_t angle)
{
    cairo_rotate(cntxt, angle);
    return cntxt;
}


// Scaling
inline draw_context const&
operator *= (draw_context const& cntxt, double scale)
{
    cairo_scale(cntxt, scale, scale);
    return cntxt;
}

//_______________________________ CAIRO PATHS ________________________________||
namespace cairo
{

// Path in namiplot format
class path
{
public:
    enum direction { direct, reverse };
private:
    void append(data_cptr const& x, data_cptr const& y,
                direction dir, unsigned xrow, unsigned yrow);

    std::vector<cairo_path_data_t> data;

    friend cairo_t * operator << (cairo_t * cairo, path const& rhs);
#ifdef TEST
public:
#endif
    cairo_path_t native_format();

    bool next_is_move = false;

public:
    path() = default;
    bool operator == (path const& rhs) const;

    path(cairo_t * context);
    path & operator = (cairo_t * context);
    path(data_cptr const& x, data_cptr const& y, unsigned row = 0)
    { append(x, y, direction::direct, 0, row); }

    path(data_cptr const& x, data_cptr const& y, direction dir, unsigned row = 0)
    { append(x, y, dir, 0, row); }

    path & operator += (path const& rhs);

    path & close();
    void reset();
};

path      operator +  (path const& lhs, path const& rhs);
cairo_t * operator << (cairo_t * cairo, path const& rhs);


//_____________________________ CAIRO PATTERNS _______________________________||

class pattern
{
private:
    cairo_pattern_t * data = nullptr;
    double w = 0;
    double h = 0;

public:
    pattern() = default;
    pattern(pattern const& other);
    pattern(pattern     && other);
    pattern & operator = (pattern const& other);
    pattern & operator = (pattern     && other);

    pattern(cairo_pattern_t * take_ownership);

    // Imports a PDF from a file and stores it in a cairo pattern.
    // Bounding box top left corner is at origin and has width() and height().
    //FIXME: pattern(std::filesystem::path const& filename);
    pattern(std::string const& filename);
    void load(std::filesystem::path const& filename);

    pattern(group_guard && group);
    pattern & operator = (group_guard && group);

    // FIXME: Weird things happen without this line, and the comparisons
    // are not working well anyway with flawed copy/move/assignment operators
    // and constructors.
    bool operator == (pattern const& other) const = default;

    ~pattern();

    operator cairo_pattern_t * () const;
    double width()  { return w; }
    double height() { return h; }
};


//_______________________________ RAII GUARDS ________________________________||

class state_guard
{
    cairo_t * cairo;
public:
    state_guard(state_guard const&) = delete;

    state_guard(cairo_t * cairo_context)
	: cairo(cairo_context)
    {
        if (!cairo_context)
            throw std::runtime_error("namiplot:__FILE__:__LINE__:"
                  "*cairo_context cannot be empty.");
	cairo_save(cairo);
    }

    ~state_guard()
    {
	cairo_restore(cairo);
    }
};


class group_guard
{
    cairo_t * cairo;

public:
    group_guard(cairo_t * cairo_context)
      : cairo {cairo_context}
    {
        if (!cairo)
            // TODO: Embed in a namiplot_bug error class
            throw "namiplot:__FILE__:__LINE__:"
                  "cairo_context cannot be empty.";
	cairo_push_group(cairo);
        cairo_reference(cairo);
    }

    ~group_guard()
    {
        if (cairo)
        {
            cairo_pop_group_to_source(cairo);
            cairo_paint(cairo);
            cairo_destroy(cairo);
        }
    }

    cairo_pattern_t * dettach()
    {
        auto ret = cairo_pop_group(cairo);
        cairo_destroy(cairo);
        cairo = nullptr;
        return ret;
    }

    operator cairo_t * () const { return cairo; }
};

} // Namespace cairo


