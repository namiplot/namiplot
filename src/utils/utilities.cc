/* Copyright 2020-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami::version {


const unsigned major = NAMIPLOT_VERSION_MAJOR;
const unsigned minor = NAMIPLOT_VERSION_MINOR;
const unsigned patch = NAMIPLOT_VERSION_PATCH;

const std::string version_string = "namiplot " NAMIPLOT_VERSION
                               " (commit " NAMIPLOT_COMMIT_DATE
                               " " NAMIPLOT_COMMIT_HASH ")";
const std::string homepage = NAMIPLOT_WEBSITE;


} // namespace nami::version

#ifdef TEST
namespace test{

void test_version()
{
    using namespace nami;
    std::cout << "Major " << version::major << std::endl;
    std::cout << "Minor " << version::minor << std::endl;
    std::cout << "Patch " << version::patch << std::endl;
    std::cout << "version_string " << version::version_string << std::endl;
    std::cout << "homepage " << version::homepage << std::endl;
        ASSERT(version::major == 20 || version::major == 23);
        ASSERT(0 <= version::minor && version::minor <= 100);
        ASSERT(0 <= version::patch && version::patch <= 1000);
        //TODO: ASSERT(version::homepage == "www.namiplot.cc");
}

void test_round()
{
    using namespace nami;
    std::cout << "Testing round...\n";

    ASSERT(round(-1.0,      round_no) == -1.0);
    ASSERT(round(-1.000001, round_no) == -1.000001);
    ASSERT(round(-0.999999, round_no) == -0.999999);
    ASSERT(round(-1.5,      round_no) == -1.5);
    ASSERT(round(-1.500001, round_no) == -1.500001);
    ASSERT(round(-1.499999, round_no) == -1.499999);
    ASSERT(round(-2.0,      round_int) == -2.0);
    ASSERT(round(-2.000001, round_int) == -2.0);
    ASSERT(round(-1.999999, round_int) == -2.0);
    ASSERT(round(-2.5,      round_int) == -2.0); //FIXME
    ASSERT(round(-2.500001, round_int) == -3.0);
    ASSERT(round(-2.499999, round_int) == -2.0);
    ASSERT(round(-3.0,      round_middle) == -2.5);
    ASSERT(round(-3.000001, round_middle) == -3.5);
    ASSERT(round(-2.999999, round_middle) == -2.5);
    ASSERT(round(-3.5,      round_middle) == -3.5);
    ASSERT(round(-3.500001, round_middle) == -3.5);
    ASSERT(round(-3.499999, round_middle) == -3.5);

    ASSERT(round(0.0,      round_no)     == 0.0);
    ASSERT(round(0.0,      round_int)    == 0.0);
    ASSERT(round(0.0,      round_middle) == 0.5);

    ASSERT(round(1.0,      round_no) == 1.0);
    ASSERT(round(1.000001, round_no) == 1.000001);
    ASSERT(round(0.999999, round_no) == 0.999999);
    ASSERT(round(1.5,      round_no) == 1.5);
    ASSERT(round(1.500001, round_no) == 1.500001);
    ASSERT(round(1.499999, round_no) == 1.499999);
    ASSERT(round(2.0,      round_int) == 2.0);
    ASSERT(round(2.000001, round_int) == 2.0);
    ASSERT(round(1.999999, round_int) == 2.0);
    ASSERT(round(2.5,      round_int) == 3.0);
    ASSERT(round(2.500001, round_int) == 3.0);
    ASSERT(round(2.499999, round_int) == 2.0);
    ASSERT(round(3.0,      round_middle) == 3.5);
    ASSERT(round(3.000001, round_middle) == 3.5);
    ASSERT(round(2.999999, round_middle) == 2.5);
    ASSERT(round(3.5,      round_middle) == 3.5);
    ASSERT(round(3.500001, round_middle) == 3.5);
    ASSERT(round(3.499999, round_middle) == 3.5);
}

void test_polymorphic_ptr()
{
    using namespace nami;
    std::cout << "Testing polymorphic_ptr...\n";

    struct aggr_no_virtual_destructor {
        int phony;
    };
    struct aggr : aggr_no_virtual_destructor {
        virtual ~aggr();
    };
    static_assert(!Cloneable<aggr>);

    struct has_clone_but_not_polymorphic : aggr_no_virtual_destructor {
        has_clone_but_not_polymorphic * clone() const;
    };
    static_assert(!Cloneable<has_clone_but_not_polymorphic>);

    struct has_clone_but_not_const : aggr {
        virtual has_clone_but_not_const * clone();
    };
    static_assert(!Cloneable<has_clone_but_not_const>);

    struct wrong_return_type : aggr {
        virtual void * clone() const;
    };
    static_assert(!Cloneable<wrong_return_type>);

    struct has_clone : aggr {
        virtual has_clone * clone() const;
    };
    static_assert( Cloneable<has_clone>);


    struct polym : aggr {
        virtual ~polym() {}
    };
    static_assert(!Cloneable<polym>);

    struct has_wrong_virtual_clone : polym {
        virtual int clone() const;
    };
    static_assert(!Cloneable<has_wrong_virtual_clone>);

    struct has_wrong_ptr_virtual_clone : polym {
        virtual int * clone() const;
    };
    static_assert(!Cloneable<has_wrong_ptr_virtual_clone>);


    struct derived_of_not_ok;
    struct clone_is_not_ok : polym {
        virtual derived_of_not_ok * clone() const;
    };
    static_assert(!Cloneable<clone_is_not_ok>);

    struct derived_of_not_ok : clone_is_not_ok {
        derived_of_not_ok * clone() const override;
    };
    static_assert( Cloneable<derived_of_not_ok>);


    struct clone_is_ok : polym {
        virtual clone_is_ok * clone() const;
    };
    static_assert( Cloneable<clone_is_ok>);
    polymorphic_ptr<clone_is_ok> ptr;
    ASSERT(!ptr.get());


    struct derived_clone_is_not_ok : clone_is_ok {
        clone_is_ok * clone() const override;
    };
    static_assert(!Cloneable<derived_clone_is_not_ok>);

    struct derived_clone_is_ok : clone_is_ok {
        derived_clone_is_ok * clone() const override;
    };
    static_assert( Cloneable<derived_clone_is_ok>);
}


template <typename T>
nami::coroutine_iterator<nami::add_const_if_const<typename T::value_type, T>>
coroutine_iterator_test(T & v)
{
    auto it = v.rbegin();
    while (it != v.rend())
    {
        co_yield *it;
        it++;
    }
}

void test_coroutine_iterator()
{
    std::cout << "Testing coroutine_iterator...\n";

    // Test mutable iterator
    std::vector v = {1, 2, 3, 4, 5};
    auto it = coroutine_iterator_test(v);
    ++it;
    ASSERT(*it == 4);
    *it += 5;
    ASSERT(*it == 9);

    // Test const iterator
    std::vector const vc = v;
    auto it2 = coroutine_iterator_test(vc);

    ASSERT(*it2 == 5); ++it2;
    ASSERT(*it2 == 9); ++it2;
    ASSERT(*it2 == 3); ++it2;
    ASSERT(*it2 == 2); ++it2;
    ASSERT(*it2 == 1); ++it2;

    ASSERT_THROW(*it2);
}


void test()
{
    test_version();
    test_round();
    test_polymorphic_ptr();
    test_coroutine_iterator();
}

} // namespace test
#endif // TEST

