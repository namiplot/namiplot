/* Copyright 2020-2021 Francisco J. Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
//#include <poppler.h>
namespace nami::cairo {


//_______________________________ CAIRO PATHS ________________________________||

using cpdt = cairo_path_data_t;
constexpr static cpdt path_header_move_to    = {CAIRO_PATH_MOVE_TO,    2};
constexpr static cpdt path_header_line_to    = {CAIRO_PATH_LINE_TO,    2};
constexpr static cpdt path_header_curve_to   = {CAIRO_PATH_CURVE_TO,   4};
constexpr static cpdt path_header_close_path = {CAIRO_PATH_CLOSE_PATH, 1};

void
path:: append(data_cptr const& x,
              data_cptr const& y,
              direction dir,
              unsigned xrow,
              unsigned yrow)
{
    if (x->cols() != y->cols())
        throw std::range_error(
                "namiplot: X and Y have incompatible dimensions");
    if (x->cols() <= 0)
        return;
    if (x->rows() <= xrow || y->rows() <= yrow) // DEBUG
        throw std::runtime_error("namiplot: Internal error in "
                "path::append()");

    data.reserve(data.size() + x->cols());

    for (unsigned precol = 0; precol < x->cols(); precol++)
    {
        unsigned col = (dir == direction::direct ?
                precol : (x->cols() - precol - 1));
        cairo_path_data_t point;
        point.point = { x->at(xrow, col), y->at(yrow, col) };

        if (std::isnan(point.point.x) || std::isnan(point.point.y))
        {
            next_is_move = true;
            continue;
        }

        data.push_back(next_is_move ? path_header_move_to
                                    : path_header_line_to);
        data.push_back(point);
        next_is_move = false;
    }
}

path &
path:: close()
{
    if (data.size() >= 2 && data[0].header.type != CAIRO_PATH_CLOSE_PATH)
    {
        data.push_back(path_header_close_path);
        data.push_back(path_header_move_to);
        cairo_path_data_t point;
        point.point = data[1].point;
        data.push_back(point);
        next_is_move = false;
    }
    return *this;
}

cairo_path_t
path:: native_format()
{
    // NOTE: The paths returned by cairo_path_copy() start with MOVE_TO.
    //       But here we are returning paths that may start with LINE_TO.
    return cairo_path_t{CAIRO_STATUS_SUCCESS, data.data(), (int)data.size()};
}

path &
path::operator = (cairo_t * context)
{
    *this = path{context};
    return *this;
}

bool
path:: operator == (path const& rhs) const
{
    if (data.size() != rhs.data.size())
        return false;

    auto lhs_it =     data.begin();
    auto rhs_it = rhs.data.begin();
    while (lhs_it != data.end())
    {
        if ( lhs_it->header.type   != rhs_it->header.type 
          || lhs_it->header.length != rhs_it->header.length )
            return false;

        for (int i = lhs_it->header.length; i > 1; --i) {
            ++lhs_it;
            ++rhs_it;
            if ( lhs_it->point.x != rhs_it->point.x
              || lhs_it->point.y != rhs_it->point.y )
            return false;
        }
        ++lhs_it;
        ++rhs_it;
    }
    return next_is_move == rhs.next_is_move;
}

path &
path:: operator += (path const& rhs)
{
    auto rhs_it = rhs.data.begin();
    if (next_is_move && !rhs.data.empty() &&
        rhs_it->header.type == CAIRO_PATH_LINE_TO)
    {
        data.push_back(*rhs_it++);
        data.back().header.type = CAIRO_PATH_MOVE_TO;
    }
    std::copy(rhs_it, rhs.data.end(), std::back_inserter(data));
    next_is_move = rhs.next_is_move;
    return *this;
}


path:: path(cairo_t * context)
{
    // CAIRO_PATH_CURVE_TO is not supported because it gives problems at the
    // extremes when adding non-continuous paths.
    cairo_path_t * cp = cairo_copy_path_flat(context);
    data.reserve(cp->num_data);
    std::copy_n(cp->data, cp->num_data, std::back_inserter(data));
    cairo_path_destroy(cp);
}

void
path::reset()
{
    *this = path { };
}

path
operator +  (path const& lhs, path const& rhs)
{
    return path{lhs} += rhs;
}

cairo_t *
operator << (cairo_t * cairo, path const& rhs)
{
    // There are noconst types in members of lvalue.
    auto lvalue = const_cast<path &>(rhs).native_format();
    cairo_append_path(cairo, &lvalue);
    return cairo;
}


//_____________________________ CAIRO PATTERNS _______________________________||

void
pattern:: load(std::filesystem::path const& filename)
{
    /*
    // * 1 *  Cairo context
    const double canvas_size = 500.;
    cairo_rectangle_t rect {0, 0, canvas_size, canvas_size};
    cairo_surface_t * surface =
        cairo_recording_surface_create( CAIRO_CONTENT_COLOR_ALPHA, &rect );
    cairo_t * cairo = cairo_create( surface );

    // * 2 *  Poppler import
    // FIXME: Error handling of Poppler functions is missing.
    std::string pdf_file = "file:";
    pdf_file += filename;
    GError * error = nullptr;
    PopplerDocument * PDF = poppler_document_new_from_file(
            pdf_file.c_str(), nullptr, &error );
    if (!PDF)
        throw std::runtime_error("libpoppler error (does the file " "exist?)");
    PopplerPage * PAGE = poppler_document_get_page(PDF, 0);

    poppler_page_get_size(PAGE, &w, &h);
    // cairo_recording_surface_ink_extents() returns all 0.
    if (w > canvas_size || h > canvas_size)
    {
        // FIXME!
        //g_object_unref(G_OBJECT(PAGE));
        //g_object_unref(G_OBJECT(PDF));
        throw std::overflow_error("Imported file does not fit in canvas size");
    }

    // * 3 *  Pattern from poppler
    {
        group_guard cg { cairo };
        poppler_page_render(PAGE, cg);
        // FIXME destroy data first.
        data = cg.dettach();
        // FIXME
        //w = ...
        //h = ...
    }
    cairo_destroy(cairo);
    cairo_surface_destroy(surface);

    // FIXME!
    //g_object_unref(G_OBJECT(PAGE));
    //g_object_unref(G_OBJECT(PDF));
    */
}


pattern:: ~pattern()
{
    if (data)
        cairo_pattern_destroy(data);
}



pattern &
pattern:: operator = (pattern const& other)
{
    if (other.data)
    {
        data = cairo_pattern_reference(other.data);
        w = other.w;
        h = other.h;
    }
    else
    {
        data = nullptr;
        w = h = 0;
    }
    return *this;
}

pattern &
pattern:: operator = (pattern && other)
{
    data = other.data;
    other.data = nullptr;

    w = other.w;
    h = other.h;
    other.w = other.h = 0;

    return *this;
}

pattern &
pattern:: operator = (group_guard && group)
{
    data = group.dettach();

    // FIXME
    //w = ...
    //h = ...

    return *this;
}

pattern::operator cairo_pattern_t * () const
{
    static auto empty_pattern = cairo_pattern_create_rgba(0, 0, 0, 0);
    return data ? data : empty_pattern;
}

pattern:: pattern(pattern const& other)
{
    *this = other;
}

pattern:: pattern(pattern && other)
{
    *this = std::move(other);
}

pattern::pattern(cairo_pattern_t * take_ownership)
{
    data = take_ownership;
    // FIXME
    //w = ...
    //h = ...
}

//FIXME: pattern:: pattern(std::filesystem::path const& filename)
pattern:: pattern(std::string const& filename)
{
    load(filename);
}

pattern:: pattern(group_guard && group)
{
    *this = std::move(group);
}

} // namespace nami::cairo

#ifdef TEST //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace test
{

std::ostream & operator << (std::ostream & out, nami::cairo::path const& c_rhs)
{
    using namespace nami;
    cairo::path rhs = c_rhs;
    auto nf = rhs.native_format();
    unsigned n_elements = 0;

    for (int i = 0; i < nf.num_data;)
    {
        switch (nf.data[i].header.type)
        {
            case CAIRO_PATH_MOVE_TO:    std::cout << "MOVE TO";    break;
            case CAIRO_PATH_LINE_TO:    std::cout << "LINE TO";    break;
            case CAIRO_PATH_CURVE_TO:   std::cout << "CURVE";      break;
            case CAIRO_PATH_CLOSE_PATH: std::cout << "CLOSE PATH"; break;
            default: throw std::runtime_error(
                             "namiplot:cairo: unknown path_header");
        }

        int i_end = i + nf.data[i].header.length;
        for (i++; i < i_end; i++) {
            std::cout << " (" << nf.data[i].point.x
                      << ", " << nf.data[i].point.y << ")";
        }
        std::cout << std::endl;
        ++n_elements;
    }
    std::cout << "*** PATH with length " << nf.num_data << " and "
        << n_elements << " elements"
        << " and next element is " << (rhs.next_is_move ? "MOVE" : "line")
        << std::endl << std::endl;

    return out;
}

void test_path (cairo_t * context)
{
    return;
    //std::cout << "Testing paths..." << std::endl;
    //using vecd = std::vector<double>;
    //using data_t = data_adaptor<vecd>;
    //constexpr double nan = std::numeric_limits<double>::quiet_NaN();

    //auto x0 = std::make_shared<data_t>(vecd{ });
    //const cairo::path xy0 {x0, x0};

    //auto x4 = std::make_shared<data_t>(vecd{  0,  1,  1, 4 });
    //auto y4 = std::make_shared<data_t>(vecd{ -3, -2, -1, 0 });
    //const cairo::path xy4 {x4, y4};

    //auto x3 = std::make_shared<data_t>(vecd{ 5, 6, 7 });
    //auto y3 = std::make_shared<data_t>(vecd{ 1, 2, 4 });
    //const cairo::path xy3 {x3, y3};

    //auto x7 = std::make_shared<data_t>(vecd{  0,  1,  1, 4, 5, 6, 7 });
    //auto y7 = std::make_shared<data_t>(vecd{ -3, -2, -1, 0, 1, 2, 4 });
    //const cairo::path xy7 {x7, y7};

    //test_regular<cairo::path>(xy7);

    //// operator +=
        //ASSERT(cairo::path {} == xy0);
        //ASSERT(xy3 != xy4);
        //ASSERT(xy4 != xy7);
        //ASSERT(xy7 != xy4);
    //cairo::path xy43 = xy4;
    //xy43 += xy3;
        //ASSERT(xy43 == xy7);
        //ASSERT(xy7 == xy43);
        //ASSERT(xy0 != xy7);
    //cairo::path xy07 = xy0;
    //xy07 += xy7;
        //ASSERT(xy07 == xy7);
    //xy07 = cairo::path {x0, x0};
        //ASSERT(xy07 == cairo::path { });
    //xy07 = xy7;
        //ASSERT(xy07 == xy7);
    //xy07 += xy0;
        //ASSERT(xy07 == xy7);

    //// operator + (binary)
    //ASSERT(xy0 + xy3 == xy3);
    //ASSERT(xy4 + xy0 == xy4);
    //ASSERT(xy4 + xy3 == xy7);
    //ASSERT(xy4 + xy0 + xy3 == xy7);

    //// nan at the border
    //auto xn = std::make_shared<data_t>(vecd{ 8.8 });
    //auto yn = std::make_shared<data_t>(vecd{ nan });
    //cairo::path xyn {xn, yn};
        //ASSERT(xyn != xy0);
        //ASSERT(xyn + xyn == xyn);

    //auto x3n = std::make_shared<data_t>(vecd{ 5, 6, 7, nan });
    //auto y3n = std::make_shared<data_t>(vecd{ 1, 2, 4, nan });
    //const cairo::path xy3n {x3n, y3n};
    //auto xn3 = std::make_shared<data_t>(vecd{ nan, 5, 6, 7 });
    //auto yn3 = std::make_shared<data_t>(vecd{ nan, 1, 2, 4 });
    //const cairo::path xyn3 {xn3, yn3};

    //ASSERT(xy3 + xyn != xy3);
    //ASSERT(xy3 + xyn != xyn3);
    //ASSERT(xy3 + xyn == xy3n);
    //ASSERT(xyn + xy3 != xy3);
    //ASSERT(xyn + xy3 == xyn3);
    //ASSERT(xyn + xy3 != xy3n);

    //// operator - (unitary)
    //auto x3r = std::make_shared<data_t>(vecd{ 7, 6, 5 });
    //auto y3r = std::make_shared<data_t>(vecd{ 4, 2, 1 });
    //const cairo::path xyr3 {x3, y3, cairo::path::reverse};
    //const cairo::path xy3r {x3r, y3r};
    //ASSERT(xy3r == xyr3);

    //const cairo::path xyn3r {x3n, y3n, cairo::path::reverse};
    //const cairo::path xy3nr {xn3, yn3, cairo::path::reverse};

    //ASSERT(xy3r+xyn == xyr3+xyn);
    //ASSERT(xyn+xy3r == xyn+xyr3);
    //ASSERT(xy3r + xyn != xy3r);
    //ASSERT(xy3r + xyn != xyn3r);
    //ASSERT(xy3r + xyn == xy3nr);
    //ASSERT(xyn + xy3r != xy3r);
    //ASSERT(xyn + xy3r == xyn3r);
    //ASSERT(xyn + xy3r != xy3nr);

    //// Get path from cairo context
    //auto x2 = std::make_shared<data_t>(std::vector<double>{ 1, 2 });
    //auto y2 = std::make_shared<data_t>(std::vector<double>{ 1, 2 });
    //cairo::path xy2 {x2, y2};
    //cairo_line_to(context, 1, 1);
    //cairo_line_to(context, 2, 2);
    //cairo::path xyc {context};
        //ASSERT(xyc == xyn + xy2); // cairo-generated paths start with MOVE_TO
    //cairo_move_to(context, 5, 1);
    //cairo_line_to(context, 6, 2);
    //cairo_line_to(context, 7, 4);
    //xyc = {context};
        //ASSERT(xyc == xyn + xy2 + xyn + xy3);
    //xyc.reset(); // reset()
        //ASSERT(xyc == xy0);
    //xyc = context;
        //ASSERT(xyc == xyn + xy2 + xyn + xy3);

    //// native_format()
    //cairo_new_path(context);
    //context << xy3;
        //ASSERT(cairo::path {context} == xyn + xy3);

    //// close()
    //cairo_close_path(context);
    //xyc = context;
        //ASSERT(xyc == (xyn + xy3).close());
    //context << xy4;
    //xyc = context;
        //ASSERT(xyc == (xyn + xy3).close() + xy4);

    //std::cout << "DEBUG: xyc " << xyc << std::endl;

    //xyc.reset();
    //xyc.close();
        //ASSERT(xyc == xy0);
}

void test_transformation (cairo_t * context)
{
    /*group cg {context}
    cairo::transformation tf;
    cairo::transformation tf_cntxt {context};

    // Rotations
    rotation_t rot;
    tf += rot;
    context += rotation;

    // Translations
    canvas_point;
    context += canvas_point

    // Scales
    double scale, canvas_t center
    context *= scale;

    context *= tf;
    */
}

void test_pattern (cairo_t * context, cairo_surface_t * surface)
{
    using namespace nami;
    std::cout << "Testing patterns..." << std::endl;
    test_regular<cairo::pattern>(cairo_pattern_create_for_surface(surface));
    cairo::pattern pattern {};
    ASSERT(cairo_pattern_status(pattern) == CAIRO_STATUS_SUCCESS);
}

void test()
{
    cairo_surface_t * surface = cairo_image_surface_create(
          CAIRO_FORMAT_A1, 10, 10);
    cairo_t * context = cairo_create(surface);

    test_path(context);
    test_transformation(context);
    test_pattern(context, surface);

    cairo_destroy(context);
    cairo_surface_destroy(surface);
}

} // namespace test
#endif // TEST

