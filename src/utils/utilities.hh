/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

// DEBUG
template <typename T>
std::ostream & operator << (std::ostream & out, std::vector<T> const& data)
{
    for (auto d : data)
        out << d << ' ';

    return out << std::endl;
}

namespace metadata
{
    extern const unsigned major;
    extern const unsigned minor;
    extern const unsigned patch;
    extern const std::string version_string;
    extern const std::string homepage;
// TODO: //license //author //email
}

//_______________________________ DRAW_CONTEXT _______________________________||

struct canvas_size_t
{
    unsigned width;
    unsigned height;
};

struct animation_info
{
    unsigned frame_count = 0;
    const double framerate = 0.; // In Hz. 0 for non-real time.
};

struct draw_context
{
    cairo_t * cairo = nullptr;

    unsigned width  = 600;
    unsigned height = 400;

    animation_info anim;

    constexpr
    operator cairo_t * () const
    { return cairo; }
};


//___________________________ GENERAL UTILITITES _____________________________||

enum rounding_specifier
{
    round_no,
    round_int,
    round_middle
};

// TODO: Avoid any rounding when exporting to vector formats, i.e. SVG, EPS.
constexpr
double
round(double value, rounding_specifier round)
{
    switch (round)
    {
    case round_int:
        return std::round(value);
    case round_middle:
        return std::floor(value) + 0.5;
    case round_no:
    default:
        return value;
    }
}


//______________________________ POLYMORPHIC_PTR _____________________________||
/* Polymorphic_ptr is a copyable unique_ptr for polymorphic types.
 * It's purpose is to recover the copy ability for value types that are
 * not available in references to polymorphic types.  */

// FIXME: Detect that clone() is virtual
template <typename T>
concept Cloneable =
    requires (T const* t) {
        {t->clone()} -> std::same_as<std::remove_const_t<T> *>;
    } &&
    std::is_polymorphic_v<T>;


// Same as std::unique_ptr, but copyable by using T.clone()
template <Cloneable T>
class polymorphic_ptr : public std::unique_ptr<T>
{
public:
    using std::unique_ptr<T>::unique_ptr;

    polymorphic_ptr(polymorphic_ptr const& copy)
      : std::unique_ptr<T> {}
    {
        *this = copy;
    }

    template <Cloneable D>
        requires std::convertible_to<D *, T *>
    polymorphic_ptr(polymorphic_ptr<D> const& copy)
      : std::unique_ptr<T> {}
    {
        *this = copy;
    }

    polymorphic_ptr(T const& copy)
      : std::unique_ptr<T> {}
    {
        this->reset(copy.clone());
    }


    polymorphic_ptr & operator = (polymorphic_ptr const& copy)
    {
        if (copy)
            this->reset(copy->clone());
        return *this;
    }

    template <typename D>
    polymorphic_ptr & operator = (polymorphic_ptr<D> const& copy)
    {
        if (copy)
            this->reset(copy->clone());
        return *this;
    }

    polymorphic_ptr & operator = (T const& copy)
    {
        this->reset(copy.clone());
        return *this;
    }
};

#define POLYMORPHIC_CLONE_OVERRIDE(CLASSNAME) \
public: \
    [[nodiscard("Ownership transferred to caller")]] \
    CLASSNAME * clone() const override \
    { return new CLASSNAME(*this); } \


//____________________________ COROUTINE_ITERATOR ____________________________||

/** Make forward iterators from coroutines
 * value_t: Type handled by the iterator
 * Use co_yield for every item in the container.
 * Use co_return (or exit function) when there are no more items.
 *
 * Copies not allowed. Move allowed.
 */
template <typename value_t>
class coroutine_iterator
{
public:
    struct promise_type;
    using handle_t = std::coroutine_handle<promise_type>;

private:
    handle_t handle;

public:
    struct promise_type
    {
        std::suspend_never initial_suspend() { return {}; }
        std::suspend_never final_suspend() noexcept { return {}; }
        void unhandled_exception() { }
        handle_t get_return_object() { return handle_t::from_promise(*this); }

        std::suspend_always yield_value(value_t & ref) {ptr = &ref; return {};}
        void return_void() { ptr = nullptr;
            std::cerr << "DEBUG return void\n"; 
        }

        value_t * ptr;

    };

public:
    coroutine_iterator(handle_t const& h)
      : handle{h}
    {}

    coroutine_iterator(coroutine_iterator const& copy) = delete;
    coroutine_iterator(coroutine_iterator     && move) = default;

    coroutine_iterator & operator = (coroutine_iterator const& copy) = delete;
    coroutine_iterator & operator = (coroutine_iterator     && move) = default;

    ~coroutine_iterator()
    {
        if (handle.promise().ptr != nullptr)
            handle.destroy();
    }

public:

    using value_type        = value_t;
    using reference         = value_t &;
    using pointer           = value_t *;
    using difference_type   = long;
    using iterator_category = std::forward_iterator_tag;

    coroutine_iterator & operator ++ ()
    {
        handle.resume();
        return *this;
    }
    coroutine_iterator operator ++ (int) = delete;

    reference operator * ()
    {
        return *operator -> ();
    }

    pointer operator -> ()
    {
        value_t * retptr = handle.promise().ptr;
        std::cerr << "DEBUG ptr " << retptr << "\n";
        if (retptr == nullptr)
            throw std::out_of_range("Attempted to dereference a sentinel");
        else
            return retptr;
    }
};

template <typename Target, typename Model>
using add_const_if_const = std::conditional_t<
    std::is_const_v<Model>,
    std::add_const_t<Target>,
    Target>;


//__________________________________ OTHER ___________________________________||

template<typename T>
struct sides
{
    T lft;
    T rgt;
    T top;
    T bot;

    sides() = default;

    //template <typename ARG>
        //requires std::convertible_to<ARG, T>
    //sides(ARG const& all_sides)
      //: lft(all_sides), rgt(all_sides), top(all_sides), bot(all_sides)
    //{ }

    sides(T const& all_sides)
      : lft(all_sides), rgt(all_sides), top(all_sides), bot(all_sides)
    { }

    sides(T const& left_right, T const& top_bottom)
      : lft(left_right), rgt(left_right), top(top_bottom), bot(top_bottom)
    { }

    sides(T const& lft_arg, T const& rgt_arg,
          T const& top_arg, T const& bot_arg)
      : lft(lft_arg), rgt(rgt_arg), top(top_arg), bot(bot_arg)
    { }


    friend
    bool operator == (sides const& lhs, sides const& rhs)
    {
        return lhs.lft == rhs.lft && lhs.rgt == rhs.rgt
            && lhs.top == rhs.top && lhs.bot == rhs.bot;
    }

    friend
    bool operator != (sides const& lhs, sides const& rhs)
    { return !(lhs == rhs); }
};


// TODO: Make it wrapper-like when reflection is available.
//       See keyword_wrapper.
template <typename T>
class appointed_vector : public std::vector<T>
{
    static inline const T default_value = {};
public:
    using std::vector<T>::vector;

    appointed_vector(auto const& value)
      : std::vector<T>(1, T(value))
    { }

    appointed_vector(appointed_vector const& value)
      : std::vector<T>(1, T(value))
    { }

    appointed_vector & operator = (auto const& value)
    {
        std::vector<T>::clear();
        std::vector<T>::push_back(T(value));
        return *this;
    }

    appointed_vector & operator = (appointed_vector const& value) = default;

    T const& looped_at(size_t pos) const
    {
        return std::vector<T>::operator[] (pos % std::vector<T>::size());
    }

    operator T const& () const
    {
       if (std::vector<T>::empty())
          return default_value;
       else
          return std::vector<T>::at(0);
    }
};

