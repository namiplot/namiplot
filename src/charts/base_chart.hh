/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

//________________________________ BASE_CHART ________________________________||

class base_chart
{
    DEFINE_ATTRIBUTE(base_chart, disable);
    DEFINE_ATTRIBUTE(base_chart, name);
    // themes are special keywords
    keyword_wrapper<decltype(nami::theme), nullptr, nullptr> theme;

protected:
    void attribute_initialization(
            decltype(nami::theme)::assigned_type const& kwrd)
    { theme = kwrd.value; }

    template <Assigned_keyword K>
    void attribute_initialization(K const& unsupported_keyword)
    {
        static_assert(!std::same_as<K,K>,
                "The following keyword is not supported in this chart:");
        static_assert(!std::same_as<K,K>, //K::unique_type_id );
            "TODO: Use non-type string template parameter");
    }


protected:
    base_chart() = default;

public:
    virtual ~base_chart() = default;

public:
    void render(cairo_surface_t * surface,
                animation_info const& animation_data
                ) const;

    void draw(draw_context & cntxt) const;
protected:
    // Instantiate this function as appropriate.
    virtual void nvi_draw(draw_context & cntxt) const = 0;

public:
    // nvi not appropriate because of covariant return types in derived classes.
    [[nodiscard("Ownership transferred to caller")]]
    virtual base_chart * clone() const = 0;
};

static_assert(!std::semiregular<base_chart>);


/*** Utilities for derived clasess ***/

template <typename C>
concept Namichart = std::is_base_of_v<base_chart, C>;

template <typename T>
concept Multichart_arguments = Namichart<T> || Assigned_keyword<T>;

using chart_ptr  = polymorphic_ptr<base_chart>;
using chart_cptr = polymorphic_ptr<const base_chart>;


