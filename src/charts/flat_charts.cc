/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {


//_________________________________ PLOT2D ___________________________________||

void
flat_chart:: adjust_canvas_axis(draw_context const& cntxt) const
{
    auto [lowx, uppx] = xy_views().x.view_limits(cntxt, *this, view_t::X);
    auto [lowy, uppy] = xy_views().y.view_limits(cntxt, *this, view_t::Y);
    if (lowx == uppx || lowy == uppy)
        throw std::range_error("view_t: Divide by zero: "
                "lower and upper limits must differ");

    cntxt += {0_pu, 1_pu};
    cairo_scale(cntxt, 1, -1);

    cairo_scale(cntxt, cntxt.width  / (uppx - lowx),
                       cntxt.height / (uppy - lowy));
    cairo_translate(cntxt, -lowx, -lowy);
}

std::vector<cairo::path>
flat_chart:: data_paths(draw_context const& cntxt, bool reversed) const
{
    auto const& X = x;
    auto const& Y = y;

    if (!Y || Y->empty())
        return {};

    if (X && !X->empty())
    {
        if (X->rows() != 1)
            throw std::range_error("X data must be single row");
        if (X->size() != Y->cols())
        {
            throw std::range_error(
                    "Namiplot: X and Y have incompatible dimensions");
        }
    }

    unsigned int y_rows = Y->rows();
    std::vector<cairo::path> paths(y_rows);
    for (unsigned i = 0; i < Y->rows(); i++)
        if (reversed)
            paths[i] = cairo::path {X, Y, cairo::path::reverse, i};
        else
            paths[i] = cairo::path {X, Y, cairo::path::direct, i};

    return paths;
}

//__________________________________ CURVE ___________________________________||

void
curve:: nvi_draw(draw_context & cntxt) const
{
    set_dash_in_cairo(cntxt, line_pattern(), width(), line_pattern_pace());
    cairo_set_line_width(cntxt, width());

    auto paths = data_paths(cntxt);

    unsigned int i = 0;
    for (auto const& path : paths)
    {
        {   cairo::group_guard gg {cntxt};
            this->adjust_canvas_axis(cntxt);
            cntxt << path;
        }
        cairo_set_source_rgba(cntxt, color().looped_at(i++) );

        cairo_stroke(cntxt);
    }
}

//__________________________________ AREA ____________________________________||

void
area:: nvi_draw(draw_context & cntxt) const
{
    auto paths = data_paths(cntxt);

    unsigned int i = 0;
    for (auto & path : paths)
    {
        {   cairo::group_guard gg {cntxt};
            this->adjust_canvas_axis(cntxt);
            cntxt << path;
        }
        cairo_set_source_rgba(cntxt, fill_color().looped_at(i++) );

        cairo_fill(cntxt);
    }
}

std::vector<cairo::path>
area:: data_paths(draw_context const& cntxt) const
{
    auto paths = flat_chart::data_paths(cntxt);
    // TODO: Ranges
    std::for_each(paths.begin(), paths.end(), [](auto & p){ p.close(); } );
    return paths;
}


//__________________________________ _SF_ ____________________________________||

//std::vector<cairo::path>
//stacked_area:: data_paths(draw_context const& cntxt) const
//{
    //auto         paths = flat_chart::data_paths(cntxt, false);
    //auto reverse_paths = flat_chart::data_paths(cntxt, true);
    //if (paths.empty())
        //return {};

    //// reverse path k is appended to direct path k+1 to form loops
    //auto rpath_it = reverse_paths.begin();
    //for (auto path_it = paths.begin(); ++path_it != paths.end(); )
         //*path_it += *rpath_it++;

    //if (fill_to_xaxis())
    //{
        //data_cptr X = std::make_unique<data_adaptor<std::vector<double>>>
            //(std::vector<double> {x->at(0,0), x->at(0, x->cols()-1)});

        //data_cptr Y = std::make_unique<data_adaptor<std::vector<double>>>
            //(std::vector<double> {0., 0.});

        //paths.front() += cairo::path {X, Y, cairo::path::reverse};
    //}
    //else
        //paths.erase(paths.begin());

    //std::for_each(paths.begin(), paths.end(), [](auto & p){ p.close(); } );
    //return paths;
//}


//_________________________________ SCATTER __________________________________||

namespace {

void symbol_preset_star(cairo_t * dc) {
    cairo_move_to(dc, -0.866,  0.5);
    cairo_line_to(dc,  0.866, -0.5);
    cairo_move_to(dc, -0.866, -0.5);
    cairo_line_to(dc,  0.866,  0.5);
    cairo_move_to(dc,  0, -1);
    cairo_line_to(dc,  0,  1);
}

void symbol_preset_circle(cairo_t * dc) {
    cairo_arc(dc, 0,0, 1, 0.,2.*std::numbers::pi);
    cairo_close_path(dc);
}

void symbol_preset_cross(cairo_t * dc) {
    cairo_move_to(dc, -1, -1);
    cairo_line_to(dc,  1,  1);
    cairo_move_to(dc, -1,  1);
    cairo_line_to(dc,  1, -1);
}

void symbol_preset_diamond(cairo_t * dc) {
    cairo_move_to(dc, -1,  0);
    cairo_line_to(dc,  0,  1);
    cairo_line_to(dc,  1,  0);
    cairo_line_to(dc,  0, -1);
    cairo_close_path(dc);
}

void symbol_preset_plus(cairo_t * dc) {
    cairo_move_to(dc, -1,  0);
    cairo_line_to(dc,  1,  0);
    cairo_move_to(dc,  0, -1);
    cairo_line_to(dc,  0,  1);
}

void symbol_preset_point(cairo_t * dc) {
    cairo_set_line_cap(dc, CAIRO_LINE_CAP_ROUND);
    cairo_line_to(dc, 0, 0);
}

void symbol_preset_square(cairo_t * dc) {
    cairo_rectangle(dc, -1,-1, 2,2);
}

void symbol_preset_tack(cairo_t * dc) {
    cairo_rectangle(dc, -1,-1, 2,2);
}


std::array<void (*)(cairo_t *), 8> const symbol_presets = {
    &symbol_preset_plus,
    &symbol_preset_circle,
    &symbol_preset_star,
    &symbol_preset_point,
    &symbol_preset_cross,
    &symbol_preset_square,
    &symbol_preset_diamond,
    &symbol_preset_tack
};

} // namespace
} // namespace nami

