/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */


// TODO: axis, with these parameters:
// color, width, location, data with a list of bases

//_______________________________ CHART_RULER ________________________________||

class ruler final : public base_chart
{
    protected: using base_chart::attribute_initialization;

public:
    ruler() = default;

    ruler(Assigned_keyword auto ... attrs)
    {
        (..., this->attribute_initialization(attrs));
    }

    ruler(base_chart * ref_chart, Assigned_keyword auto ... attrs)
    {
        (..., this->attribute_initialization(attrs));
        reference_chart = ref_chart;
    }

    POLYMORPHIC_CLONE_OVERRIDE(ruler)

protected:
    void nvi_draw(draw_context & cntxt) const override;

private:
    polymorphic_ptr<flat_chart> reference_chart;

public:
    std::vector<std::shared_ptr<text>> ruler_labels();
};


//_______________________________ CHART_GRID _________________________________||

class grid final : public base_chart
{
    DEFINE_ATTRIBUTE(grid, color);
    DEFINE_ATTRIBUTE(grid, line_pattern);
    // width is fixed at 1_px

    protected: using base_chart::attribute_initialization;

public:
    grid() = default;

    grid(flat_chart const& ref_chart, Assigned_keyword auto ... attrs)
      : views_chart {ref_chart}
    {
        (..., this->attribute_initialization(attrs));
    }

    grid(std::string_view ref_name, Assigned_keyword auto ... attrs)
    {
        (..., this->attribute_initialization(attrs));
    }

    POLYMORPHIC_CLONE_OVERRIDE(grid)

protected:
    void nvi_draw(draw_context & cntxt) const override;

private:
    polymorphic_ptr<flat_chart const> views_chart;
};

//__________________________________ FRAME ___________________________________||

class frame final : public base_inset
{
public:
    DEFINE_ATTRIBUTE(frame, title);
    DEFINE_ATTRIBUTE(frame, xlabel);
    DEFINE_ATTRIBUTE(frame, ylabel);

    protected: using base_inset::attribute_initialization;

public:
    frame() = default;

    frame (frame const& copy)
      : base_inset (copy)
    {
        //this->subplot      = copy.subplot->clone();
        this->title  = copy.title;
        this->xlabel = copy.xlabel;
        this->ylabel = copy.ylabel;
    }

    //template <Namiplot P>
    //frame (P const& inset_pl,
           //Assigned_keyword auto ... attrs)
      //: frame(attrs...)
    //{
        //inset_plot = std::make_unique<P>(inset_pl);
    //}
    POLYMORPHIC_CLONE_OVERRIDE(frame)

protected:
    // Looks for the first data plot for ruler and grid,
    // without looking recursively.
    void nvi_draw(draw_context & cntxt) const override;
};
