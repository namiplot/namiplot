/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

//_______________________________ PLOT_IN_PLOT _______________________________||

class base_inset : public base_chart
{
    DEFINE_ATTRIBUTE(base_inset, background);
    DEFINE_ATTRIBUTE(base_inset, border_color);
    DEFINE_ATTRIBUTE(base_inset, border_width);
    DEFINE_ATTRIBUTE(base_inset, no_clip);
    DEFINE_ATTRIBUTE(base_inset, margins);

    protected: using base_chart::attribute_initialization;

protected:
    base_inset() = default;

    base_inset(chart_ptr const& chart)
      : inset_chart { chart }
    { }

    void set(chart_ptr const& chart)
    { inset_chart = chart; }

public:
    [[nodiscard("Ownership transferred to caller")]]
    virtual base_inset * clone() const = 0;

protected:
    void draw_inset(draw_context & cntxt) const;

private:
    chart_ptr inset_chart;
};


//__________________________________ INSET ___________________________________||

class inset final : public base_inset
{
    protected: using base_inset::attribute_initialization;

private:
    void initialize_subchart(stack & init_stack,
                             Namichart auto const& chart_n,
                             Multichart_arguments auto ... attrs)
    {
        init_stack.push_back(chart_n);
        initialize_subchart(init_stack, attrs...);
    }

    void initialize_subchart(stack & init_stack,
                             Assigned_keyword auto ... attrs)
    {
        if (init_stack.size() == 1)
            this->set(std::make_unique<stack>(init_stack[0]));
        else if (init_stack.size() > 1)
            this->set(std::make_unique<stack>(init_stack));

        (..., this->attribute_initialization(attrs));
    }

public:
    inset() = default;

    inset(Multichart_arguments auto ... attrs)
    {
        stack init_stack;
        initialize_subchart(init_stack, attrs...);
    }

    POLYMORPHIC_CLONE_OVERRIDE(inset)

protected:
    void nvi_draw(draw_context & cntxt) const override;
};

// FIXME
//static_assert(std::semiregular<inset>);

