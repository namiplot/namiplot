/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {

//_______________________________ PLOT_VECTOR ________________________________||


// TODO
//template <>
//chart_ptr
//vector_chart:: at<base_chart>(size_t pos) const
//{
    //if (pos < plots.size())
        //return plots.at(pos);
    //else
        //return {};
//}

void
vector_chart:: clear()
{
    return plots.clear();
}


bool
vector_chart:: empty() const
{
    return plots.empty();
}


void
vector_chart:: pop_back()
{
    plots.pop_back();
}

void
vector_chart:: pop_front()
{
    plots.erase(plots.begin());
}


size_t
vector_chart:: size() const
{
    return plots.size();
}


//_________________________________ STACK ____________________________________||


void
stack:: nvi_draw(draw_context & cntxt) const
{
    // TODO: for (auto const& plot_p : ranges::view::reverse(intVec)
    for (auto it = crbegin(); it != crend(); ++it)
        it->draw(cntxt);
}


//_________________________________ GRILLE ___________________________________||

// TODO: Use C++20 ranges.
std::vector<double>
normalized_dims(std::vector<double> ratios, unsigned int const n)
{
    ratios.resize(n);
    if (n == 0)
        return ratios;

    std::for_each(ratios.begin(), ratios.end(), [](double & ratio)
    {
        if (ratio <= 0.)
        ratio = 1.;
    });

    // Clamp ratios bigger than 10x the smaller ratio.
    double max = *std::min_element(ratios.begin(), ratios.end()) * 10.;
    std::for_each(ratios.begin(), ratios.end(), [max](double & ratio)
    {
        ratio = std::min(ratio, max);
    });

    // Normalize.
    double total = std::accumulate(ratios.begin(), ratios.end(), 0.);
    double sum = 0.;
    std::for_each(ratios.begin(), ratios.end(), [total, &sum](double & ratio)
    {
        sum += ratio / total;
        ratio = sum;
    });

    ratios.insert(ratios.begin(), 0.);
    return ratios;
}

void
grille:: nvi_draw(draw_context & cntxt) const
{
    unsigned const c_cols =
        std::min<unsigned int>(n_cols(), size());
    unsigned const c_rows = n_rows();

    std::vector ratios_w_s = normalized_dims(ratios_cols(), c_cols);
    std::vector ratios_h_s = normalized_dims(ratios_rows(), c_rows);

    //for (unsigned j = 0; j < c_rows; j++)
        //for (unsigned i = 0; i < c_cols; i++)
        {
	    //unsigned sub_index = j*c_cols + i;
            // TODO
	    //if (sub_index < size()) {
		//inset subplot{ operator[](sub_index),
                               //border_width=0,
                               //margins={0_pu,1_pu,0_pu,1_pu}
                               //};
		//subplot.margins.lft.perunit += ratios_w_s[i];
		//subplot.margins.rgt.perunit -= ratios_w_s[i+1];
		//subplot.margins.top.perunit += ratios_h_s[j];
		//subplot.margins.bot.perunit -= ratios_h_s[j+1];
                //subplot.draw(cntxt);
	    //}
	}
}

unsigned
grille:: n_rows() const
{
    unsigned rows = size() / n_cols();
    if (rows * n_cols() != size())
	rows++;
    return rows;
}

} // namespace nami

