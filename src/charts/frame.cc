/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {


static
double
round_tozero_0125 (double x)
{
    using namespace std;
    if (x == 0.) return 0.;

    int digits = floor(log10(abs(x)));
    double ret = pow(10, digits);

    if      (5*ret <= abs(x)) ret *= 5;
    else if (2*ret <= abs(x)) ret *= 2;

    return (x < 0 ? -1 : 1) * ret;
}

#ifdef TEST
} // namespace nami
namespace test {

void test_floor_0125()
{
    std::cerr << "Testing round_tozero_0125()...\n";

    ASSERT(round_tozero_0125(-.0001)  == -.0001);
    ASSERT(round_tozero_0125(-.0002)  == -.0002);
    ASSERT(round_tozero_0125(-.0003)  == -.0002);
    ASSERT(round_tozero_0125(-.0005)  == -.0005);
    ASSERT(round_tozero_0125(-.0008)  == -.0005);
    ASSERT(round_tozero_0125(-1.)     == -1.);
    ASSERT(round_tozero_0125(-2.)     == -2.);
    ASSERT(round_tozero_0125(-3.)     == -2.);
    ASSERT(round_tozero_0125(-5.)     == -5.);
    ASSERT(round_tozero_0125(-8.)     == -5.);
    ASSERT(round_tozero_0125(-10.)    == -10.);
    ASSERT(round_tozero_0125(-20.)    == -20.);
    ASSERT(round_tozero_0125(-30.)    == -20.);
    ASSERT(round_tozero_0125(-50.)    == -50.);
    ASSERT(round_tozero_0125(-80.)    == -50.);
    ASSERT(round_tozero_0125(-1000.)  == -1000.);
    ASSERT(round_tozero_0125(-2000.)  == -2000.);
    ASSERT(round_tozero_0125(-3000.)  == -2000.);
    ASSERT(round_tozero_0125(-5000.)  == -5000.);
    ASSERT(round_tozero_0125(-8000.)  == -5000.);

    ASSERT(round_tozero_0125(-0.99)   == -0.5);
    ASSERT(round_tozero_0125(-1.)     == -1.);
    ASSERT(round_tozero_0125(-1.0001) == -1.);
    ASSERT(round_tozero_0125(-4.99)   == -2.);
    ASSERT(round_tozero_0125(-5.)     == -5.);
    ASSERT(round_tozero_0125(-5.0001) == -5.);

    ASSERT(round_tozero_0125(0.)      == 0.);
    ASSERT(round_tozero_0125(.0001)   == .0001);
    ASSERT(round_tozero_0125(.0002)   == .0002);
    ASSERT(round_tozero_0125(.0003)   == .0002);
    ASSERT(round_tozero_0125(.0005)   == .0005);
    ASSERT(round_tozero_0125(.0008)   == .0005);
    ASSERT(round_tozero_0125(.5)      == .5);
    ASSERT(round_tozero_0125(1.)      == 1.);
    ASSERT(round_tozero_0125(2.)      == 2.);
    ASSERT(round_tozero_0125(3.)      == 2.);
    ASSERT(round_tozero_0125(5.)      == 5.);
    ASSERT(round_tozero_0125(8.)      == 5.);
    ASSERT(round_tozero_0125(10.)     == 10.);
    ASSERT(round_tozero_0125(20.)     == 20.);
    ASSERT(round_tozero_0125(30.)     == 20.);
    ASSERT(round_tozero_0125(50.)     == 50.);
    ASSERT(round_tozero_0125(80.)     == 50.);
    ASSERT(round_tozero_0125(1000.)   == 1000.);
    ASSERT(round_tozero_0125(2000.)   == 2000.);
    ASSERT(round_tozero_0125(3000.)   == 2000.);
    ASSERT(round_tozero_0125(5000.)   == 5000.);
    ASSERT(round_tozero_0125(8000.)   == 5000.);

    ASSERT(round_tozero_0125(0.99)    == 0.5);
    ASSERT(round_tozero_0125(1.)      == 1.);
    ASSERT(round_tozero_0125(1.0001)  == 1.);
    ASSERT(round_tozero_0125(4.99)    == 2.);
    ASSERT(round_tozero_0125(5.)      == 5.);
    ASSERT(round_tozero_0125(5.0001)  == 5.);
}

} // namespace test
namespace nami {
#endif


static
std::vector<double>
inside_ticks(
        std::pair<double,double> min_max_value,
        unsigned min_ticks
        )
{
    const double min = min_max_value.first;
    const double max = min_max_value.second;
    const double data_diff = max - min;

    if (max <= min)
        throw std::range_error("inside_ticks(): min is not smaller than max");
    if (min_ticks < 2)
        throw std::range_error("inside_ticks(): min_ticks must be at least 2");

    std::vector<double> ret;
    ret.reserve( 2*min_ticks );

    const double interval = round_tozero_0125( data_diff / min_ticks );
    const double min_tick = interval * static_cast<int>(min / interval);

    for (int i = 0; (min_tick + i * interval) < max; i++)
        ret.push_back(min_tick + i * interval);

    ret.insert(ret.begin(), ret.front() - interval);
    ret.push_back(ret.back() + interval);

    while (ret.front() < min)
        ret.erase(ret.begin());
    while (ret.back() > max)
        ret.pop_back();

    return ret;
}

#ifdef TEST
} // namespace nami
namespace test {

void test_inside_ticks()
{
    // TODO: Refactor into a loop
    std::cerr << "Testing inside_ticks()...\n";

    using vd = std::vector<double>;
    vd vdtest;

    ASSERT_THROW(inside_ticks({1, 0}, 1));
    ASSERT_THROW(inside_ticks({5, 5}, 1));
    ASSERT_THROW(inside_ticks({0, 1}, 0));
    ASSERT_THROW(inside_ticks({0, 1}, 1));

    const vd vd2_01  {0, 1};
    const vd vd3_01  {0, 0.5, 1};
    const vd vd6_01  {0, 0.2, 0.4, 0.6, 0.8, 1};
    const vd vd11_01 {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1};

    vdtest = inside_ticks({0, 1}, 2);
        ASSERT(vdtest == vd2_01 || vdtest == vd3_01);
    vdtest = inside_ticks({0, 1}, 3);
        ASSERT(vdtest == vd3_01 || vdtest == vd6_01);
    vdtest = inside_ticks({0, 1}, 4);
        ASSERT(vdtest == vd6_01);
    vdtest = inside_ticks({0, 1}, 5);
        ASSERT(vdtest == vd6_01);
    vdtest = inside_ticks({0, 1}, 6);
        ASSERT(vdtest == vd6_01 || vdtest == vd11_01);
    vdtest = inside_ticks({0, 1}, 7);
        ASSERT(vdtest == vd11_01);
    vdtest = inside_ticks({0, 1}, 8);
        ASSERT(vdtest == vd11_01);
    vdtest = inside_ticks({0, 1}, 9);
        ASSERT(vdtest == vd11_01);
    vdtest = inside_ticks({0, 1}, 10);
        ASSERT(vdtest == vd11_01);

    const vd vd2_0099 {0, 0.5};
    const vd vd3_0099 {0, 0.2, 0.4, 0.6, 0.8};
    const vd vd6_0099 {0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};

    vdtest = inside_ticks({-0.01, 0.99}, 2);
        ASSERT(vdtest == vd2_0099 || vdtest == vd3_0099);
    vdtest = inside_ticks({-0.01, 0.99}, 3);
        ASSERT(vdtest == vd3_0099);
    vdtest = inside_ticks({-0.01, 0.99}, 4);
        ASSERT(vdtest == vd3_0099 || vdtest == vd6_0099);
    vdtest = inside_ticks({-0.01, 0.99}, 5);
        ASSERT(vdtest == vd3_0099 || vdtest == vd6_0099);
    vdtest = inside_ticks({-0.01, 0.99}, 6);
        ASSERT(vdtest == vd6_0099);
    vdtest = inside_ticks({-0.01, 0.99}, 7);
        ASSERT(vdtest == vd6_0099);
    vdtest = inside_ticks({-0.01, 0.99}, 8);
        ASSERT(vdtest == vd6_0099);
    vdtest = inside_ticks({-0.01, 0.99}, 9);
        ASSERT(vdtest == vd6_0099);
    vdtest = inside_ticks({-0.01, 0.99}, 10);
        ASSERT(vdtest == vd6_0099);

    const vd vd2_0101 {0.5, 1};
    const vd vd3_0101 {0.2, 0.4, 0.6, 0.8, 1};
    const vd vd6_0101 {0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1};

    vdtest = inside_ticks({0.01, 1.01}, 2);
        ASSERT(vdtest == vd2_0101 || vdtest == vd3_0101);
    vdtest = inside_ticks({0.01, 1.01}, 3);
        ASSERT(vdtest == vd3_0101);
    vdtest = inside_ticks({0.01, 1.01}, 4);
        ASSERT(vdtest == vd3_0101 || vdtest == vd6_0101);
    vdtest = inside_ticks({0.01, 1.01}, 5);
        ASSERT(vdtest == vd3_0101 || vdtest == vd6_0101);
    vdtest = inside_ticks({0.01, 1.01}, 6);
        ASSERT(vdtest == vd6_0101);
    vdtest = inside_ticks({0.01, 1.01}, 7);
        ASSERT(vdtest == vd6_0101);
    vdtest = inside_ticks({0.01, 1.01}, 8);
        ASSERT(vdtest == vd6_0101);
    vdtest = inside_ticks({0.01, 1.01}, 9);
        ASSERT(vdtest == vd6_0101);
    vdtest = inside_ticks({0.01, 1.01}, 10);
        ASSERT(vdtest == vd6_0101);

    const vd vd2_105  {-10, -5};
    const vd vd3_105  {-10, -8, -6};
    const vd vd6_105  {-10, -9, -8, -7, -6, -5};
    const vd vd11_105 {-10, -9.5, -9, -8.5, -8, -7.5, -7, -6.5, -6, -5.5, -5};

    vdtest = inside_ticks({-10, -5}, 2);
        ASSERT(vdtest == vd2_105 || vdtest == vd3_105);
    vdtest = inside_ticks({-10, -5}, 3);
        ASSERT(vdtest == vd3_105 || vdtest == vd6_105);
    vdtest = inside_ticks({-10, -5}, 4);
        ASSERT(vdtest == vd6_105);
    vdtest = inside_ticks({-10, -5}, 5);
        ASSERT(vdtest == vd6_105);
    vdtest = inside_ticks({-10, -5}, 6);
        ASSERT(vdtest == vd6_105 || vdtest == vd11_105);
    vdtest = inside_ticks({-10, -5}, 7);
        ASSERT(vdtest == vd11_105);
    vdtest = inside_ticks({-10, -5}, 8);
        ASSERT(vdtest == vd11_105);
    vdtest = inside_ticks({-10, -5}, 9);
        ASSERT(vdtest == vd11_105);
    vdtest = inside_ticks({-10, -5}, 10);
        ASSERT(vdtest == vd11_105);
}

} // namespace test
namespace nami {
#endif


//_______________________________ CHART_RULER ________________________________||

void
ruler:: nvi_draw(draw_context & cntxt) const
{
    /*
    views vw = dynamic_cast_views(plot_with_views);

    std::vector<double> ticks_x = inside_ticks(
            vw.x.pixel_to_data(0),
            vw.x.pixel_to_data(cntxt.width),
            cntxt.width / 100); // TODO: Logarithmic rule?
    std::vector<double> ticks_y = inside_ticks(
            vw.y.pixel_to_data(0),
            vw.y.pixel_to_data(cntxt.height),
            cntxt.height / 100); // TODO: Logarithmic rule?

    // Ticks
    for (auto tick : ticks_x)
    {
        cairo_move_to(
            cntxt,
            round(vw.x.scale(tick), round_middle),
            round(cntxt.height,     round_middle));
        cairo_line_to(
            cntxt,
            round(vw.x.scale(tick),   round_middle),
            round(cntxt.height - 4.5, round_middle));
    }
    for (auto tick : ticks_y)
    {
        cairo_move_to(
            cntxt,
            round(0,                               round_middle),
            round(cntxt.height - vw.y.scale(tick), round_middle));
        cairo_line_to(
            cntxt,
            round(4.5,                             round_middle),
            round(cntxt.height - vw.y.scale(tick), round_middle));
    }

    cairo_set_source_rgba(cntxt, black);
    cairo_set_line_width(cntxt, 1);
    cairo_stroke(cntxt);

    for (double tick : ticks_x)
    {
        std::ostringstream sstr;
        sstr << tick;

        canvas_point text_pos = {0_px, 10_px};
        text_pos.x.pixels = vw.x.scale(tick);
    }
    for (double tick : ticks_y)
    {
        std::ostringstream sstr;
        sstr << tick;

        canvas_point text_pos;
        text_pos.x.pixels = -10; // Avoid unary operator - ();
        text_pos.y.pixels = -vw.y.scale(tick);
    }
    */
}


std::vector<std::shared_ptr<text>>
ruler:: ruler_labels()
{
    return {};
}


//_______________________________ CHART_GRID _________________________________||


void
grid:: nvi_draw(draw_context & cntxt) const
{
    using namespace ::nami;

    if (!views_chart) return;

    // TODO: Use attribute type std::function<vector<double> (min, max, width)>
    std::vector<double> ticks_x = inside_ticks(
        views_chart->xy_views().x.view_limits(cntxt, *views_chart, view_t::X),
        log(cntxt.width / 15) + 1
        );
    std::vector<double> ticks_y = inside_ticks(
        views_chart->xy_views().y.view_limits(cntxt, *views_chart, view_t::Y),
        log(cntxt.height / 15) + 1
        );

    {
        cairo::group_guard gg {cntxt};
        views_chart->adjust_canvas_axis(cntxt);

        double null;
        for (auto & tick : ticks_x)
            cairo_user_to_device(cntxt, &tick, &null);
        for (auto & tick : ticks_y)
            cairo_user_to_device(cntxt, &null, &tick);
    }

    for (auto tick : ticks_x)
    {
        ::cairo_move_to(
            cntxt,
            round(tick,         round_middle),
            round(0,            round_middle));
        ::cairo_line_to(
            cntxt,
            round(tick,         round_middle),
            round(cntxt.height, round_middle));
    }
    for (auto tick : ticks_y)
    {
        ::cairo_move_to(
            cntxt,
            round(0,                   round_middle),
            round(cntxt.height - tick, round_middle));
        ::cairo_line_to(
            cntxt,
            round(cntxt.width,         round_middle),
            round(cntxt.height - tick, round_middle));
    }

    cairo_set_source_rgba(cntxt, color());
    cairo_set_line_width(cntxt, 1.);

    std::vector<double> line_dash = line_pattern();
    cairo_set_dash(cntxt, line_dash.data(), line_dash.size(), 0.);

    cairo_stroke(cntxt);
}


//__________________________________ FRAME ___________________________________||

//void
//frame:: nvi_draw(draw_context & cntxt) const
//{
    // TODO
    //auto views_chart = framed_stack->at<PARAMETER_TYPE(x_view)>(0);

    //if (views_chart)
    //{
        //auto framed_ruler = std::make_shared<ruler>(views_chart);
        //// TODO: Optional grid, specified by parameter.
        //auto framed_grid = std::make_shared<grid>(views_chart);

        //base_chart::draw(cntxt,
                //inset{
                    //stack{framed_ruler,
                          //framed_grid,
                          //framed_stack},
                    //::background = background,
                    //::margins = margins }
                    //);

        //for (auto text_ptr : framed_ruler->ruler_labels())
        //{
            //text_ptr->coord.x        += margins.lft;
            //text_ptr->coord.y.pixels += cntxt.height;
            //text_ptr->coord.y        -= margins.bot;
            //base_chart::draw(cntxt, *text_ptr);
        //}
    //}
    //else
    //{
        //base_chart::draw(cntxt, inset{(stack)*framed_stack, ::margins=margins});
    //}

    //inset::nvi_draw(cntxt);

    // Axis labels and title
    //double const x_axis_midpoint =
        //(cntxt.width  + (margins().lft - margins().rgt).value(cntxt.width )) /2;
    //double const y_axis_midpoint =
        //(cntxt.height + (margins().top - margins().bot).value(cntxt.height)) /2;

    //base_chart::draw(cntxt, text
        //{ title, font_size=18, font_align=BBOX_C, coord=
            //{ px(x_axis_midpoint)
            //, px(margins().top.value(cntxt.height) / 2)
            //}
        //} );

    //base_chart::draw(cntxt, text
        //{ xlabel, font_align=N, coord=
            //{ px(x_axis_midpoint)
            //, px(cntxt.height) - margins().bot + 35_px
            //}
        //} );

    //base_chart::draw(cntxt, text
        //{ ylabel, font_align=S, rotation=90_deg, coord=
            //{ margins().lft - 45_px
            //, px(y_axis_midpoint)
            //}
        //} );
//}

} // namespace nami

#ifdef TEST
namespace test {

void test()
{
    test_floor_0125();
    test_inside_ticks();
}

}
#endif
