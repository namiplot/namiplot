/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {

//FIXME: These definitions should go in themes.cc but it does not compile.
const theme_t namitheme {};
theme_t const* primary_theme = &default_primary_theme;

//________________________________ BASE_CHART ________________________________||

void
base_chart:: draw(draw_context & cntxt) const
{
    if (disable())
        return;

    cairo::state_guard st{cntxt};
    theme_t const* recovery_theme = primary_theme;
    primary_theme = &theme();

    nvi_draw(cntxt);

    primary_theme = recovery_theme;
}

void
base_chart:: render(cairo_surface_t * surface,
                     animation_info const& animation_data
                     ) const
{
    draw_context cntxt = {
        .cairo = cairo_create(surface),
        .anim  = animation_data };

    switch (cairo_surface_get_type(surface))
    {
      case CAIRO_SURFACE_TYPE_XLIB:
        cntxt.width  = cairo_xlib_surface_get_width(surface);
        cntxt.height = cairo_xlib_surface_get_height(surface);
        break;
      case CAIRO_SURFACE_TYPE_IMAGE:
        cntxt.width  = cairo_image_surface_get_width(surface);
        cntxt.height = cairo_image_surface_get_height(surface);
        break;
      default:
        throw std::runtime_error("base_chart::render(): "
                "Cairo surface type not supported.");
    }

    if (theme.delegate())
        primary_theme = &default_primary_theme;
    else
        primary_theme = &static_cast<theme_t const&>(theme);

    {cairo::group_guard cg(cntxt);
        cairo_set_source_rgb(cntxt, theme().canvas.color);
        cairo_paint(cntxt);
        draw(cntxt);
    }

    cairo_destroy(cntxt);
}

} // namespace nami

