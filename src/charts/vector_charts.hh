/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

//_______________________________ PLOT_VECTOR ________________________________||


// Abstract class
class vector_chart : public base_chart
{
    protected: using base_chart::attribute_initialization;

protected: /* Constructors */
    vector_chart() = default;

    template <Namichart C>
    vector_chart (C const& insert_chart)
    {
        push_back(insert_chart);
    }


private:
    using vector_of_plots = std::vector< chart_ptr >;
public: /* Iterators */

    template <typename base_iterator>
    class vector_chart_iterator : public base_iterator
    {
    public:
        using value_type        = base_chart &;
        using reference         = base_chart &;
        using pointer           = chart_ptr;
        using difference_type   = long;
        using iterator_category = std::random_access_iterator_tag;

        value_type operator * ()
        { return *base_iterator::operator*(); }
        pointer operator -> ()
        { return *base_iterator::operator->(); }

        vector_chart_iterator(base_iterator it)
            : base_iterator(it) {}
    };

    using iterator =
        vector_chart_iterator<vector_of_plots::iterator>;

    iterator begin () { return iterator {plots.begin()}; }
    iterator end   () { return iterator {plots.end()};   }


    using reverse_iterator =
        vector_chart_iterator<vector_of_plots::reverse_iterator>;

    reverse_iterator rbegin () { return reverse_iterator {plots.rbegin()}; }
    reverse_iterator rend   () { return reverse_iterator {plots.rend()};   }


    template <typename base_iterator>
    class vector_chart_const_iterator : public base_iterator
    {
    public:
        using value_type        = base_chart const&;
        using reference         = base_chart const&;
        using pointer           = base_chart const*; // FIXME: chart_cptr
        using difference_type   = long;
        using iterator_category = std::random_access_iterator_tag;

        value_type operator * () const
        { return *base_iterator::operator*(); }
        pointer operator -> () const
        { return base_iterator::operator->()->get(); }

        vector_chart_const_iterator(base_iterator it)
            : base_iterator(it) {}
    };

    using const_iterator =
        vector_chart_const_iterator<vector_of_plots::const_iterator>;

    const_iterator begin  () const { return const_iterator {plots.begin()};  }
    const_iterator end    () const { return const_iterator {plots.end()};    }
    const_iterator cbegin () const { return const_iterator {plots.cbegin()}; }
    const_iterator cend   () const { return const_iterator {plots.cend()};   }


    using const_reverse_iterator =
        vector_chart_const_iterator<vector_of_plots::const_reverse_iterator>;
    private: using cri = const_reverse_iterator; public:

    cri rbegin  () const { return cri {plots.rbegin()};  }
    cri rend    () const { return cri {plots.rend()};    }
    cri crbegin () const { return cri {plots.crbegin()}; }
    cri crend   () const { return cri {plots.crend()};   }


public:

    template <Namichart P>
    base_chart const& push_front (P const& plot_obj);
    template <Namichart P>
    base_chart const& push_back  (P const& plot_obj);

    void pop_front ();
    void pop_back  ();

    void   clear();

    size_t size()  const;
    bool   empty() const;


    base_chart &
    operator[] (size_t pos)
    { return *plots[pos]; }

    base_chart const&
    operator[] (size_t pos) const
    { return *plots[pos]; }

    // TODO: Use Ranges and then return const& and non-const
    // variety: reference not possible if plot not found.
    //template <typename T>
    //base_chart *           at(size_t pos) const;

    //template <Namiplot_bare P = base_chart>
    //std::shared_ptr<P> at(size_t pos) const;


    // ***************** Wrapping of plots.
    /* TODO Named Requirements of Container
    using vector_of_plots::get_allocator;
    using vector_of_plots::front;
    using vector_of_plots::back;
    using vector_of_plots::data;
    using vector_of_plots::assign;
    using vector_of_plots::pop_back;
    using vector_of_plots::erase;
    using vector_of_plots::resize;
    using vector_of_plots::swap;
    using vector_of_plots::insert;
    using vector_of_plots::emplace;
    using vector_of_plots::emplace_back;
    */
    // max_size
    // reserve
    // capacity
    // shrink_to_fit

private:
    vector_of_plots plots;

};


/* ********** TEMPLATE FUNCTIONS ********** */


//template <typename T>
//chart_ptr
//vector_chart:: at(size_t pos) const
//{
    //for (auto const& it : plots)
    //{
        //std::shared_ptr<T> ptr = std::dynamic_pointer_cast<T>(it);
        //if (ptr) {
            //if (pos == 0)
                //return it;
            //else
                //--pos;
        //}
    //}
    //return {};
//}

//template <Namiplot_bare P>
//std::shared_ptr<P>
//vector_chart:: at(size_t pos) const
//{
    //for (auto const& it : plots)
    //{
        //std::shared_ptr<P> ptr = std::dynamic_pointer_cast<P>(it);
        //if (ptr) {
            //if (pos == 0)
                //return ptr;
            //else
                //--pos;
        //}
    //}
    //return {};
//}

//template <>
//chart_ptr
//vector_chart:: at<base_chart>(size_t pos) const;


template <Namichart C>
base_chart const&
vector_chart:: push_back (C const& insert)
{
    plots.emplace_back(insert);
    return *plots.back();
}


template <Namichart C>
base_chart const&
vector_chart:: push_front (C const& insert)
{
    plots.emplace(plots.begin(), insert);
    return *plots.front();
}


//_________________________________ STACK ____________________________________||

class stack final : public vector_chart
{
    protected: using vector_chart::attribute_initialization;
public: /* Constructors */
    stack() = default;

    stack(Namichart            auto const& chart,
          Multichart_arguments auto  ...   attrs)
      : stack(attrs...)
    {
        vector_chart::push_front(chart);
    }

    stack(Assigned_keyword auto ... attrs)
    {
        (..., this->attribute_initialization(attrs));
    }

    POLYMORPHIC_CLONE_OVERRIDE(stack)

protected:
    void nvi_draw(draw_context & cntxt) const override;
};


//_________________________________ GRILLE ___________________________________||

class grille final : public vector_chart
{
    DEFINE_ATTRIBUTE(grille, n_cols);
    DEFINE_ATTRIBUTE(grille, ratios_cols);
    DEFINE_ATTRIBUTE(grille, ratios_rows);

    protected: using vector_chart::attribute_initialization;

public:
    grille() = default;

    grille(Namichart        auto const& chart,
           Assigned_keyword auto  ...   attrs)
      : grille(attrs...)
    {
        push_front(chart);
    }

    POLYMORPHIC_CLONE_OVERRIDE(grille)

protected:
    void nvi_draw(draw_context & cntxt) const override;

public:
    unsigned int n_rows() const;
};

