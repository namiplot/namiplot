/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

//___________________________________ TEXT ___________________________________||

class text final : public base_chart
{
    DEFINE_ATTRIBUTE(text, background);
    DEFINE_ATTRIBUTE(text, color);
    DEFINE_ATTRIBUTE(text, coord);
    DEFINE_ATTRIBUTE(text, font_align);
    DEFINE_ATTRIBUTE(text, font_size);
    DEFINE_ATTRIBUTE(text, rotation);

    protected: using base_chart::attribute_initialization;

public:
    text() = default;

    text(std::string const& text_str, Assigned_keyword auto ... attrs)
    {
        (..., this->attribute_initialization(attrs));
        text_string = text_str;
    }

    POLYMORPHIC_CLONE_OVERRIDE(text)

    //canvas_point extents(cairo_t * cairo);
protected:
    void nvi_draw(draw_context & cntxt) const override;

protected:
    std::string text_string;
};


//_____________________________ LATEX_EXCEPTION ______________________________||
#if 0
class latex_exception : public std::runtime_error
{
public:
    const std::string latex_log;

    explicit
    latex_exception(std::string const& what_arg,
                    std::string const& latex_log_arg );
    explicit
    latex_exception(char        const* what_arg,
                    std::string const& latex_log_arg );
};


//___________________________________ LATEX __________________________________||

// TODO: Executors when available
class latex : public text
{
protected: /* Common virtual functions. */
    void nvi_draw(draw_context & cntxt) override;

public: /* Plot-specific constructors */
    latex(latex const &) = default;

    template<typename ... param_args_t>
    latex(  canvas_point xy_reference,
            std::string latex_math,
            param_args_t ... param_args)
      : text(xy_reference, latex_math)
    {
        REGISTER_PARAMETER(header);
        REGISTER_PARAMETER(math_style);
        plot::init(param_args...);
        texpattern = latex_to_pattern();
    }

private: /* Plot-specific members */
    PDF_pattern_t latex_to_pattern();

    PDF_pattern_t texpattern;

public: /* Public members */
    std::string header = "\\usepackage{amsmath} \n";
    math_style_t math_style = inlinemath;

};


#endif
