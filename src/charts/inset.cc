/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {


//__________________________________ INSET ___________________________________||

void
base_inset:: draw_inset(draw_context & cntxt) const
{
    margins_t const& tm = margins();
    double lft = round(tm.lft.value(cntxt.width),           round_middle);
    double rgt = round((1_pu - tm.rgt).value(cntxt.width),  round_middle);
    double top = round((1_pu - tm.top).value(cntxt.height), round_middle);
    double bot = round(tm.bot.value(cntxt.height),          round_middle);
    double lwidth = rgt - lft;
    double height = top - bot;

    cairo_set_source_rgba(cntxt, background());
    cairo_rectangle(cntxt, lft, bot, lwidth, height);
    cairo_fill(cntxt);

    if (inset_chart)
    {
        const bool clip = !no_clip();
        cairo_surface_t * inset_surface = nullptr;
        draw_context inset_cntxt = cntxt;

        if (clip)
        {
            inset_surface = cairo_surface_create_for_rectangle(
                    cairo_get_group_target(cntxt), lft, bot, lwidth, height );
            inset_cntxt.cairo  = cairo_create(inset_surface);
        }
        else
            inset_cntxt += {tm.lft, tm.top};

        inset_cntxt.width  = static_cast<unsigned>(lwidth);
        inset_cntxt.height = static_cast<unsigned>(height);

        inset_chart->draw(inset_cntxt);

        if (clip)
        {
            cairo_destroy(inset_cntxt);
            cairo_surface_destroy(inset_surface);
        }
    }

    if (border_width() > 0.)
    {
        cairo_set_source_rgba(cntxt, border_color());
        double const& bw  = border_width();
        cairo_set_line_width(cntxt, bw);
        cairo_rectangle(cntxt, lft-bw/2, bot-bw/2, lwidth+bw, height+bw);
        cairo_stroke(cntxt);
    }
}

void
inset:: nvi_draw(draw_context & cntxt) const
{
    this->draw_inset(cntxt);
}

} // namespace nami

