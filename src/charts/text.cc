/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>
namespace nami {

text_align_t
rotation_alignment (double radians)
{
    double const& pi = std::numbers::pi;

    while   (radians < 0)       radians += 2*pi;
    while   (radians >= 2*pi)   radians -= 2*pi;

         if (radians < pi/8)    return BBOX_W;
    else if (radians < 3*pi/8)  return SW;
    else if (radians < 5*pi/8)  return S;
    else if (radians < 7*pi/8)  return SE;
    else if (radians < 9*pi/8)  return BBOX_E;
    else if (radians < 11*pi/8) return NE;
    else if (radians < 13*pi/8) return N;
    else if (radians < 15*pi/8) return NW;
    else                        return BBOX_W;
}

//___________________________________ TEXT ___________________________________||

void
text:: nvi_draw(draw_context & cntxt) const
{
    cairo::group_guard gg {cntxt};

    // TODO: Use FAST antialias (see cairo_font_options_set_antialias) when
    //       rendering animations.

    cairo_set_source_rgba(cntxt, color());
    cairo_set_font_size(cntxt, font_size());
    cairo_select_font_face(cntxt,
                           "sans-serif",
                           CAIRO_FONT_SLANT_NORMAL,
                           CAIRO_FONT_WEIGHT_NORMAL);

    // Calculate relative translation of the text origin for alignment.
    double xrel = 0, yrel = 0;
    cairo_text_extents_t t_ext;
    cairo_font_extents_t f_ext;

    cairo_text_extents(cntxt, text_string.c_str(), &t_ext);
    cairo_font_extents(cntxt, &f_ext);

    switch(font_align())
    {
    case BBOX_NW:
        xrel = t_ext.x_bearing;
        yrel = t_ext.y_bearing;
        break;
    case BBOX_N:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = t_ext.y_bearing;
        break;
    case BBOX_NE:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = t_ext.y_bearing;
        break;
    case BBOX_W:
        xrel = t_ext.x_bearing;
        yrel = t_ext.y_bearing + t_ext.height/2;
        break;
    case BBOX_C:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = t_ext.y_bearing + t_ext.height/2;
        break;
    case BBOX_E:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = t_ext.y_bearing + t_ext.height/2;
        break;
    case BBOX_SW:
        xrel = t_ext.x_bearing;
        yrel = t_ext.y_bearing + t_ext.height;
        break;
    case BBOX_S:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = t_ext.y_bearing + t_ext.height;
        break;
    case BBOX_SE:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = t_ext.y_bearing + t_ext.height;
        break;

    case NW:
        xrel = t_ext.x_bearing;
        yrel = -f_ext.ascent;
        break;
    case N:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = -f_ext.ascent;
        break;
    case NE:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = -f_ext.ascent;
        break;
    case W:
        xrel = t_ext.x_bearing;
        yrel = 0;
        break;
    case C:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = 0;
        break;
    case E:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = 0;
        break;
    case SW:
        xrel = t_ext.x_bearing;
        yrel = f_ext.descent;
        break;
    case S:
        xrel = t_ext.x_bearing + t_ext.width/2;
        yrel = f_ext.descent;
        break;
    case SE:
        xrel = t_ext.x_bearing + t_ext.width;
        yrel = f_ext.descent;
        break;
    default:
        break;
    };

    auto xy = coord();
    cairo_translate(cntxt, xy.x.value(cntxt.width), xy.y.value(cntxt.height));

    cairo_rotate   (cntxt, -rotation());
    cairo_translate(cntxt, -xrel, -yrel);

    {cairo::group_guard ggg {cntxt};
        cairo_set_source_rgba(cntxt, background());
        cairo_rectangle(cntxt, t_ext.x_bearing, t_ext.y_bearing,
                               t_ext.height,    t_ext.height);
        cairo_fill(cntxt);
    }

    cairo_set_source_rgba(cntxt, color());
    cairo_show_text(cntxt, text_string.c_str());
    cairo_new_path(cntxt);
}

#if 0
canvas_point
text:: extents(cairo_t * cntxt)
{
    cairo_text_extents_t t_ext;
    { cairo_substate ss (cntxt);
        cairo_set_font_size(cntxt, font_size);
        cairo_select_font_face(cntxt,
                "sans-serif",
                CAIRO_FONT_SLANT_NORMAL,
                CAIRO_FONT_WEIGHT_NORMAL);
        cairo_text_extents(cntxt, text_string.c_str(), &t_ext);
    }

    canvas_point ret;
    ret.x.pixels = t_ext.width;
    ret.y.pixels = t_ext.height;
    return ret;
}



//_____________________________ LATEX_EXCEPTION ______________________________||


latex_exception:: latex_exception(std::string const& what_arg,
                                  std::string const& latex_log_arg )
    : std::runtime_error(what_arg),
      latex_log(latex_log_arg)
{ }

latex_exception:: latex_exception(char        const* what_arg,
                                  std::string const& latex_log_arg )
    : std::runtime_error(what_arg),
      latex_log(latex_log_arg)
{ }

//___________________________________ LATEX __________________________________||

#include <fstream>
#include <iostream>

#include <sys/wait.h>
#include <unistd.h>

void
latex:: nvi_draw(draw_context & cntxt)
{
    const double latex_ratio = 338./318.;
    try
    {
        const double factor = latex_ratio * font_size/12.;
        cairo_scale(cntxt, factor, factor);

        const double w2 = texpattern.width()  / 2. * factor;
        const double h2 = texpattern.height() / 2. * factor;

        // Adjust location
        double xrel = -w2;
        double yrel = -h2;
        switch(font_align)
        {
        case BBOX_NW:
        case NW:
            xrel += w2;
            yrel += h2;
            break;
        case BBOX_N:
        case N:
            yrel += h2;
            break;
        case BBOX_NE:
        case NE:
            xrel -= w2;
            yrel += h2;
            break;
        case BBOX_W:
        case W:
            xrel += w2;
            break;
        case BBOX_C:
        case C:
            break;
        case BBOX_E:
        case E:
            xrel -= w2;
            break;
        case BBOX_SW:
        case SW:
            xrel += w2;
            yrel -= h2;
            break;
        case BBOX_S:
        case S:
            yrel -= h2;
            break;
        case BBOX_SE:
        case SE:
            xrel -= w2;
            yrel -= h2;
            break;
        default:
            break;
        };
        cairo_translate(cntxt,
                (coord.x.value(cntxt.width)  + xrel) / factor,
                (coord.y.value(cntxt.height) + yrel) / factor
                );

        cairo_set_source_rgba(cntxt, color);
        texpattern.cairo_mask(cntxt);
    }
    catch (std::exception & err)
    {
        std::cout << "namiplot:latex_render():"<< err.what() << std::endl;
    }
}

// TODO: Make async if animation.
// Returns width and height of the rendered LaTeX text.
PDF_pattern_t
latex:: latex_to_pattern()
{
    namespace fs = std::filesystem;

    fs::path    tmp_prefix    = fs::temp_directory_path()/
                    ("namiplot-" + std::to_string(getpid()) + "/");
    std::string filename_root = "namiplot";
    std::string tex_file      = tmp_prefix/ (filename_root+".tex");

    // ** 1 **  Build the tex file.
    {
        fs::create_directory(tmp_prefix);
        std::ofstream namitex
                  {tex_file, std::ios_base::out | std::ios_base::trunc};

        // Remove the equation environment tags if present.
        std::string math_eq = text_string;
        if (math_eq.front() == '$' && math_eq.back() == '$') {
            math_eq.erase(math_eq.begin());
            math_eq.erase(math_eq.end()-1);
        }

        namitex << "\\documentclass[border=0.5pt,12pt]{standalone} \n"
                << header
                << "\\begin{document} \n"
                << ( math_style == inlinemath ? "$ " : "$\\displaystyle " )
                << math_eq
                << " $ \n"
                << "\\end{document} \n" ;
    }

    // ** 2 **  Execute pdflatex.
    try
    {
        int pid = fork();
        if (pid == -1)
                throw std::runtime_error("fork failed");
        if (pid)
        {
            int wstatus = 0;
            if (  waitpid(pid, &wstatus, 0) == -1
               || !WIFEXITED(wstatus)
               || WEXITSTATUS(wstatus) )
            {
                throw std::runtime_error("LaTeX process failed");
            }
        }
        else // The child process
        {
            fs::current_path(tmp_prefix);

            if (!freopen("/dev/null", "r", stdin )) exit(-1);
            if (!freopen("/dev/null", "w", stdout)) exit(-1);
            if (!freopen("/dev/null", "w", stderr)) exit(-1);

            if (!execl( PDFLATEX_COMPILER,
                        PDFLATEX_COMPILER,
                        "-halt-on-error",
                        tex_file.c_str(),
                        nullptr ))
            {
                exit(-1);
            }
        }
    }
    catch (std::exception const& error)
    {
        std::ifstream logfile { tmp_prefix/ (filename_root+".log"),
                                std::ios_base::in };
        std::stringbuf logbuf;
        logfile >> &logbuf;
        std::filesystem::remove_all(tmp_prefix);
        throw latex_exception { error.what(), logbuf.str() };
    }

    // ** 3 **
    PDF_pattern_t latex_text {tmp_prefix/(filename_root+".pdf")};
    std::filesystem::remove_all(tmp_prefix);
    return latex_text;
}

#endif

} // namespace nami

