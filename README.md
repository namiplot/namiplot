# NamiPlot

Plotting library for C++

Features:
- Quick and short creation of plots.
- Convenient plot animations.
- Clear separation between plot preparation and data calculation.
- No more cluttering of your code with plotting calls.
- Easy integration with your favorite matrix library.
  - STL vectors
  - STL vector of vectors
  - Armadillo
  - Eigen
  - Blaze
  - OpenCV
  - Function plots
  - Files
- Cairo based.
- No-throw guarantee.
- Strong compile-time type checking.
- Consistent API.
- Thread safe: Change parameters on the fly and immediately see the effect on the plot.
- [GNU/Linux] Run on remote desktops - No OpenGL dependency.
- Use C++ as a declarative language to define the plots.
- Supplies a small concise but expressive API that makes easy to construct complex plots from the base ones.
- Animated plots out of the box with parameters: Make important data stand out on screen.

