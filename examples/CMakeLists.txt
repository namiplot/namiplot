# Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
#
# This file is part of namiplot.
#
# Namiplot is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Namiplot is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
#

find_package(Armadillo REQUIRED 9.800.3)

add_executable             (functions data_types/functions.cc)
target_link_libraries      (functions namiplot)

add_executable             (namidevel namidevel.cc)
target_include_directories (namidevel PRIVATE ${ARMADILLO_INCLUDE_DIRS})
target_link_libraries      (namidevel namiplot ${ARMADILLO_LIBRARIES})
