/* Copyright 2019-2021 Paco Arjonilla Garcia <pacoarjonilla@yahoo.es>
 *
 * This file is part of namiplot.
 *
 * Namiplot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Namiplot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with namiplot.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <namiplot>

#include <armadillo>

#include <unistd.h>
#include <functional>

double cuad(double t)
{
    return t*t;
}

using namespace std::literals::chrono_literals;
using namespace nami;

int
main()
{
    arma::mat x = arma::linspace(0, 6.5, 501); x = x.t();
    arma::mat cosx = sin(x+5);
    arma::mat y;
    for (int i = 0; i < 8; i++)
        y.insert_rows(i, cosx + 0.2*i+log(i+1));


    arma::mat expx = exp(x/5000);
    curve c { exp(x/2).eval(), (y+1.).eval(), xy_views=loose, width=3 };
    //stacked_area sf { exp(x/2).eval(), (y+1.).eval()
        //, width = 3
        //, x_view = {-5., 5}
        //, y_view = {-5., 5}
        //, close_path
        //, disable
        //, fill_to_xaxis
        //};

    std::function fct(&cuad);

    std::vector<float> x0 = {-1, 0, 0, 0, 0, 1};
    std::vector<float> y0 = {0, 0, 1, -1, 0, 0};

    std::vector<float> xv = {-1, -3./5, -1./5, 1./5, 3./5, 1};
    std::vector<std::vector<double>> yv = {
        {0, 0.1, 0.25, 0.45, 0.70, 1},
        {-1, 0.1, 0.2, 0.3, 0.4, 0.6}
    };

    // FIXME: Do not allow adimensional time periods "line_pattern_pace=1"
    curve origin (x0, y0, line_pattern={9,27}, line_pattern_pace=500);
    curve f (xv, yv);
    // FIXME: Bad function call
    //curve g { std::function<double(double)>()
    //stacked_area g { [](double t){return sin(2*t)*100 + 250;}
           //, [&phi](double t){return cos(5*t + phi)*100+300;}
           //, x
           //, fill_color=color_t{0.4, teal}
           //};
    //curve h { [](double t){return sin(2*t)*100 + 250;}
           //, [&phi](double t){return cos(5*t + phi)*100+300;}
           //, x
           //, color=teal
           //, name = "curva"
           //};

    grid gr(c);

    inset ins {f
        , text{"[P]",
               font_size=150,
               background=snow,
               font_align=BBOX_SE,
               coord={0_pct,0_pct},
               rotation=20_deg}
        , c
        , gr
        , background=white
        , border_width=5
        //, no_clip
        , border_color=darkgrey
        , margins=10_pct
    };

    //auto co = inset(f, c, h, sf); // FIXME
    // FIXME: ins not show, ruler labels blinking
    //auto co = stack(text{"Plot, Nami", name="Plot, Nami"}
                    //, ins, f);
    //curve polytest = {{0, 200, 200},{0, 0, 100}};

    //frame myplot { co
        //, title = "namiplot"
        //, xlabel = "Exponentially-stretched X Axis"
        //, ylabel = "Sinusoid on Y Axis"
    //};

    auto nw = plot(ins);
    //window {stack{c,gr}};

    // FIXME: Cuando la ventana del plot es pequeno, peta.

    while (nw.active())
    //for (int i = 0; i < 30; i++)
            usleep(100000);

    return 0;
}

